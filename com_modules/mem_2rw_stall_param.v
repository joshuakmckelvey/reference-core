// Parameterised True Dual Port RAM With Stall
// Reads new value on same port, old value on different ports
module mem_2rw_stall_param (

  input clock,
  input [(DATA_WIDTH-1):0] data_a, data_b,
  input [(ADDR_WIDTH-1):0] address_a, address_b,
  input addressstall_a,
  input wren_a, wren_b,
  output reg [(DATA_WIDTH-1):0] q_a, q_b

);

  parameter FILE = "";
  parameter ADDR_WIDTH = 14-2;
  parameter DATA_WIDTH = 32;

  reg [(ADDR_WIDTH-1):0] address_a_buf;
  reg [(ADDR_WIDTH-1):0] address_a_in;


  // Declare the RAM variable
  reg [DATA_WIDTH-1:0] ram[0:2**ADDR_WIDTH-1];

  integer i;

  initial begin

    if (FILE != "") begin

      $readmemh(FILE, ram);
      
    end else begin
      
      for (i = 0; i < 2**ADDR_WIDTH-1; i = i + 1)
        ram[i] <= 0;

    end

  end

  // Port A address stall buffer
  always @(*) begin
    
    if (addressstall_a)
      address_a_in = address_a_buf;
    else
      address_a_in = address_a;

  end

  always @ (posedge clock) begin
    
    if (!addressstall_a)
      address_a_buf <= address_a;

  end

  always @ (posedge clock) begin // Port A
    if (wren_a) begin
      ram[address_a_in] <= data_a;
      q_a <= data_a;
    end else
      q_a <= ram[address_a_in];
  end

  always @ (posedge clock) begin // Port B
    if (wren_b) begin
      ram[address_b] <= data_b;
      q_b <= data_b;
    end else
      q_b <= ram[address_b];
  end

endmodule
