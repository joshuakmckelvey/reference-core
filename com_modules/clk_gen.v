module clk_gen (clk_in, ss_in, sel, clk_out);
  input clk_in, ss_in, sel;
  output clk_out;

  reg [31:0] count = 32'b0;

  parameter DIV = 32'b0;

  wire res_clk;

  // Glitchless clock multiplexer
  clk_mux   CLKMUX(.clk_0(res_clk),
                   .clk_1(ss_in),
                   .sel(sel),
                   .clk_out(clk_out));

  always @(posedge clk_in) begin

    count <= count + 1'b1;

    if (count >= (DIV-1))
      count <= 0;

  end

  assign res_clk = (count < DIV/2) ? 1'b0 : 1'b1;

endmodule
