// Main memory
module mem (clk_i, en_i, data_i, addr_i, dir_i, val_i, data_o);

  parameter FILE = "";
  parameter ADDR_WIDTH = 13;

  input clk_i, en_i;
  input [31:0] data_i;
  input [31:0] addr_i;
  input [3:0] val_i;

  input dir_i;

  output [31:0] data_o;

  reg [(ADDR_WIDTH-1):2] read_addr;

  initial begin
    if (FILE != "") begin
      $readmemh({FILE, "_0.dat"}, mem0);
      $readmemh({FILE, "_1.dat"}, mem1);
      $readmemh({FILE, "_2.dat"}, mem2);
      $readmemh({FILE, "_3.dat"}, mem3);
    end

  end

  // Main Memory
  reg [7:0] mem0 [0:((1<<ADDR_WIDTH)-1)];
  reg [7:0] mem1 [0:((1<<ADDR_WIDTH)-1)];
  reg [7:0] mem2 [0:((1<<ADDR_WIDTH)-1)];
  reg [7:0] mem3 [0:((1<<ADDR_WIDTH)-1)];

  always @(posedge clk_i) begin
    
    if (dir_i && en_i && val_i[3])
      mem0[addr_i[(ADDR_WIDTH-1):2]] <= data_i[31:24];

    if (dir_i && en_i && val_i[2])
      mem1[addr_i[(ADDR_WIDTH-1):2]] <= data_i[23:16];

    if (dir_i && en_i && val_i[1])
      mem2[addr_i[(ADDR_WIDTH-1):2]] <= data_i[15:8];

    if (dir_i && en_i && val_i[0])
      mem3[addr_i[(ADDR_WIDTH-1):2]] <= data_i[7:0];


    read_addr <= addr_i[(ADDR_WIDTH-1):2];

  end

  // Outputs currently ignore valid
  assign data_o[31:24] = mem0[read_addr];
  assign data_o[23:16] = mem1[read_addr];
  assign data_o[15:8]  = mem2[read_addr];
  assign data_o[7:0]   = mem3[read_addr];

endmodule
