// Glitchless clock multiplexer
module clk_mux (clk_0, clk_1, sel, clk_out);
  input clk_0, clk_1, sel;
  output clk_out;

  reg c0_0, c0_1, c1_0, c1_1;

  always @(posedge clk_1) begin

    c1_0 <= sel && !c0_1;

  end

  always @(negedge clk_1) begin

    c1_1 <= c1_0;

  end

  always @(posedge clk_0) begin

    c0_0 <= !c1_1 && !sel;

  end

  always @(negedge clk_0) begin

    c0_1 <= c0_0;

  end

  assign clk_out = (c1_1 && clk_1) || (c0_1 && clk_0);

endmodule
