// Data cache
// Port 0 is read only, port 1 is read/write
module dcache (clk_i,  addr_0_i, addr_1_i, data_1_i, //port_0_ctrl,
               port_1_dir, stall_0, byte_en_1_i, data_0_o, data_1_o);

  parameter FILE = "";
  parameter ADDR_WIDTH = 12;

  input clk_i;
  input [31:0] addr_0_i, addr_1_i;
  input [31:0] data_1_i;
  input [3:0] byte_en_1_i;

  //input [2:0] port_0_ctrl;  // Load Control (F3)
  input port_1_dir;
  input stall_0;              // Stalls Port 0 (RD) Address

  output [31:0] data_0_o, data_1_o;



  mem_2rw_8x1k    MEM0 (.address_a  (addr_0_i[11:2]),
                        .address_b  (addr_1_i[11:2]),
                        .clock      (clk_i),
                        .data_a     (8'b0),
                        .data_b     (data_1_i[7:0]),
                        .addressstall_a (stall_0),
                        .wren_a     (1'b0), // Read only
                        .wren_b     (port_1_dir && byte_en_1_i[0]),
                        .q_a        (data_0_o[7:0]),
                        .q_b        (data_1_o[7:0]));

  mem_2rw_8x1k    MEM1 (.address_a  (addr_0_i[11:2]),
                        .address_b  (addr_1_i[11:2]),
                        .clock      (clk_i),
                        .data_a     (8'b0),
                        .data_b     (data_1_i[15:8]),
                        .addressstall_a (stall_0),
                        .wren_a     (1'b0), // Read only
                        .wren_b     (port_1_dir && byte_en_1_i[1]),
                        .q_a        (data_0_o[15:8]),
                        .q_b        (data_1_o[15:8]));

  mem_2rw_8x1k    MEM2 (.address_a  (addr_0_i[11:2]),
                        .address_b  (addr_1_i[11:2]),
                        .clock      (clk_i),
                        .data_a     (8'b0),
                        .data_b     (data_1_i[23:16]),
                        .addressstall_a (stall_0),
                        .wren_a     (1'b0), // Read only
                        .wren_b     (port_1_dir && byte_en_1_i[2]),
                        .q_a        (data_0_o[23:16]),
                        .q_b        (data_1_o[23:16]));

  mem_2rw_8x1k    MEM3 (.address_a  (addr_0_i[11:2]),
                        .address_b  (addr_1_i[11:2]),
                        .clock      (clk_i),
                        .data_a     (8'b0),
                        .data_b     (data_1_i[31:24]),
                        .addressstall_a (stall_0),
                        .wren_a     (1'b0), // Read only
                        .wren_b     (port_1_dir && byte_en_1_i[3]),
                        .q_a        (data_0_o[31:24]),
                        .q_b        (data_1_o[31:24]));

endmodule
