// Testing module for kirv soc

`define ZERO 32'd0

module kirv_soc_tb ();
    
  reg clk;

  kirv_soc SYSTEM (.CLOCK_50  (clk),
                   .switch_i  (16'b0),
                   .key_i     (4'b1111),
                   .uart_rxd  (1'b0),   // UART Is not simulated currently
                   .hex_disp  (),
                   .rled_reg_o(),
                   .uart_txd  (),
                   .lcd_data_o(),
                   .lcd_ctrl_o());

 
  integer j;

  initial begin
      
    $dumpfile ("kirv_soc_tb.vcd");
    $dumpvars (0, kirv_soc_tb);
    
    /*
    for(j=1; j < 32; j=j+1)
        $dumpvars(SYSTEM.CPU.DATAPATH.RF.rf[j]);
        */
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[0]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[1]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[2]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[3]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[4]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[5]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[6]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[7]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[8]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[9]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[10]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[11]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[12]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[13]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[14]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[15]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[16]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[17]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[18]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[19]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[20]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[21]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[22]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[23]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[24]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[25]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[26]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[27]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[28]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[29]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[30]);
      $dumpvars(1, SYSTEM.CPU.DATAPATH.RF.RF_P0.ram[31]);


    #1000000 $finish;

  end


  always begin
      
      #5 clk = 1'b0;
      #5 clk = 1'b1;

  end

endmodule
