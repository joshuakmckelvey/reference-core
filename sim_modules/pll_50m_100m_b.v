// No clock division for simulation

module pll_50m_100m (
	inclk0,
	c0);

	input	  inclk0;
	output	  c0;

  assign c0 = inclk0;

endmodule
