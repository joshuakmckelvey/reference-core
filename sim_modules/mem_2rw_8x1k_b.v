module mem_2rw_8x1k (
	address_a,
	address_b,
	addressstall_a,
	clock,
	data_a,
	data_b,
	wren_a,
	wren_b,
	q_a,
	q_b);

	input	[9:0]  address_a;
	input	[9:0]  address_b;
	input	  addressstall_a;
	input	  clock;
	input	[7:0]  data_a;
	input	[7:0]  data_b;
	input	  wren_a;
	input	  wren_b;
	output	[7:0]  q_a;
	output	[7:0]  q_b;


  reg [9:0] address_buf_a;
  reg [9:0] address_buf_b;
  integer i;

  initial begin

    address_buf_a <= 10'b0;
    address_buf_b <= 10'b0;

    for (i = 0; i < 1024; i = i + 1)
      mem[i] <= 8'b0;

  end

  reg [7:0] mem [0:1023];


  always @(posedge clock) begin
    
    if (wren_a)
      mem[address_a] <= data_a;

    if (wren_b)
      mem[address_b] <= data_b;

    if (!addressstall_a)
      address_buf_a <= address_a;

    address_buf_b <= address_b;

  end

  assign q_a = mem[address_buf_a];
  assign q_b = mem[address_buf_b];


endmodule
