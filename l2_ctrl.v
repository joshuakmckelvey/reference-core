// L2 Cache Controller (L2 <-> MEM)
module l2_ctrl (

  input clk_i,

  // L1 Controller Signals
  input l1c_req_i,
  input l2_evict_i,

  // Memory and Peripheral input valid signals
  input mem_val_i,

  // Memory address control signals
  output reg mem_addr_reg_ld_o,     // Load external (Mem) address reg
  output reg mem_addr_reg_inc_o,    // Increment external address reg (by 4)

  // L2 Address register signals
  output reg l2_addr_ld_o,          // Load L2 address register
  output reg l2_addr_inc_o,         // Increment L2 address reg (by 4)

  // Memory bus control signals
  output reg mem_in_reg_ld_o,       // Load memory input register
  output reg mem_out_reg_ld_o,      // Load memory output register
  output reg mem_en_o,
  output reg mem_we_o,              // Memory write enable

  // L2 Cache control signals
  output reg l2_mux_o,
  output reg l2_we_o,
  output reg l2_val_o,
  output reg l2_dty_o,
  output reg l2_tag_we_o,

  output [6:0] debug_o
);

  parameter IDLE = 3'b000, L2_FILL = 3'b010, L2_EVICT = 3'b011;

  // Internal state registers
  reg [2:0] state;      // Current state
  reg [2:0] next_state; // Next state
  reg [4:0] stage;      // Current stage (progress) of the current state

  reg stage_res;        // Stage reset

  reg finish;           // Finish sequence

  reg mem_wait;         // Halts stage until mem is ready

  assign debug_o = {state, stage};

  // Next state logic
  always @(*) begin
    
    if (state == IDLE) begin

      if (l1c_req_i) begin
        
        // Check L2 fill or evict
        // Requesting L2 cache line evict
        if (l2_evict_i) begin

          next_state = L2_EVICT;
          stage_res = 1'b1;

        // Requesting L2 refill
        end else begin

          next_state = L2_FILL;
          stage_res = 1'b1;

        end

      end else begin

        next_state = IDLE;
        stage_res = 1'b1;

      end

    end else begin

      if (finish) begin
        next_state = IDLE;
        stage_res = 1'b1;
      end else begin
        next_state = state;
        stage_res = 1'b0;
      end

    end


  end

  // State transitions
  always @(posedge clk_i) begin
    
    state <= next_state;

    if (stage_res) begin

      stage <= 5'h0;

    end else if (!mem_val_i && mem_wait) begin

      stage <= stage; // Stay on current stage

    end else begin

      stage <= stage + 1;

    end

  end

  // Initializes state
  initial begin
    
    state <= IDLE;
    stage <= 5'h0;

  end

  // Control output logic
  always @(*) begin
    
    // Defaults
    mem_wait          = 1'b0;

    mem_addr_reg_ld_o = 1'b0;
    mem_addr_reg_inc_o= 1'b0;
    l2_addr_ld_o      = 1'b0;
    l2_addr_inc_o     = 1'b0;
    mem_in_reg_ld_o   = 1'b0;
    mem_out_reg_ld_o  = 1'b0;
    mem_en_o          = 1'b0;
    mem_we_o          = 1'b0;
    l2_mux_o          = 1'b0;
    l2_we_o           = 1'b0;
    l2_val_o          = 1'b0;
    l2_dty_o          = 1'b0;
    l2_tag_we_o       = 1'b0;
    finish            = 1'b0;

    case (state)
      IDLE: begin
      end
      L2_FILL: begin
        l2_mux_o          = 1'b1;
        case (stage)
          5'h0: begin   // Load address
            mem_en_o          = 1'b1;
            mem_we_o          = 1'b0; // RD Request
            l2_addr_ld_o      = 1'b1;
            mem_addr_reg_ld_o = 1'b1;
          end
          5'h1: begin   // Wait for propagation
            mem_en_o          = 1'b0;
            //mem_addr_reg_inc_o= 1'b1;
          end
          5'h2: begin   // Load word into data buffer reg
            mem_en_o          = 1'b0;
            mem_in_reg_ld_o   = 1'b1;
            mem_wait          = 1'b1;
          end
          5'h3: begin   // Load word 0 into L2 cache
            mem_en_o          = 1'b0;
            mem_in_reg_ld_o   = 1'b1;
            l2_addr_inc_o     = 1'b1;
            l2_we_o           = 1'b1;
          end
          5'h4: begin   // Load word 1 into L2 cache
            //mem_en_o          = 1'b1;
            mem_in_reg_ld_o   = 1'b1;
            l2_addr_inc_o     = 1'b1;
            l2_we_o           = 1'b1;
          end
          5'h5: begin   // Load word 2 into L2 cache
            mem_in_reg_ld_o   = 1'b1;
            l2_addr_inc_o     = 1'b1;
            l2_we_o           = 1'b1;
          end
          5'h6: begin   // Load word 3 into L2 cache and validate L2 line
            l2_we_o           = 1'b1;
            l2_val_o          = 1'b1;
            l2_dty_o          = 1'b0;
            l2_tag_we_o       = 1'b1;
          end
          5'h7: begin   // Wait for l2 to propagate TODO: remove state
            finish            = 1'b1;
          end
          default: begin  // Invalid state
          end
        endcase
      end
      L2_EVICT: begin
        l2_mux_o          = 1'b1;
        case (stage)
          5'h0: begin   // Load address
            mem_en_o          = 1'b1;
            mem_we_o          = 1'b1; // WR Request
            l2_addr_ld_o      = 1'b1;
            mem_addr_reg_ld_o = 1'b1;
          end
          5'h1: begin   // Wait for propagation
            mem_en_o          = 1'b0;
            mem_we_o          = 1'b1; // WR Request
            //mem_addr_reg_inc_o= 1'b1;
          end
          5'h2: begin   // Wait for memory to ack
            mem_en_o          = 1'b0;
            mem_we_o          = 1'b1; // WR Request
            mem_wait          = 1'b1;
          end
          5'h3: begin   // Load word 0 into memory
            mem_en_o          = 1'b0;
            l2_addr_inc_o     = 1'b1;
          end
          5'h4: begin   // Load word 1 into memory
            //mem_en_o          = 1'b1;
            l2_addr_inc_o     = 1'b1;
          end
          5'h5: begin   // Load word 2 into memory
            l2_addr_inc_o     = 1'b1;
          end
          5'h6: begin   // Load word 3 into memory
          end
          5'h7: begin   // Invalidate L2 line
            l2_we_o           = 1'b1;
            l2_val_o          = 1'b0;
            l2_dty_o          = 1'b0;
            l2_tag_we_o       = 1'b1;
          end
          5'h8: begin   // Wait for l2 dty to propagate
            finish            = 1'b1;
          end
          default: begin  // Invalid state
          end
        endcase
      end
      default: begin  // Invalid state
      end
    endcase

  end


endmodule
