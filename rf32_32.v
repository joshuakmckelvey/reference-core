// 32 bit X 32 register file
module rf32_32 (clk, wr_port, wr_en, wr_sel, rd_a_sel, rd_b_sel,
                rd_a_port, rd_b_port);
  
  input clk, wr_en;
  input [31:0] wr_port;

  input [4:0] wr_sel, rd_a_sel, rd_b_sel;

  output reg [31:0] rd_a_port, rd_b_port;

  reg [31:0] rd_a_port_buf, rd_b_port_buf;

  reg [31:0] rf [0:31];

  // Write port logic
  always @ (posedge clk) begin

    if (wr_en)
      rf[wr_sel] = wr_port;

  end

  // Read port logic
  always @ (posedge clk) begin
    
    rd_a_port_buf = rf[rd_a_sel];

    rd_b_port_buf = rf[rd_b_sel];
    
  end

  // Read port logic
  always @ (posedge clk) begin
    
    if (rd_a_sel == 5'b00000)
        rd_a_port = 32'h00000000;
    else
        rd_a_port = rd_a_port_buf;
    
    if (rd_b_sel == 5'b00000)
        rd_b_port = 32'h00000000;
    else
        rd_b_port = rd_b_port_buf;
    
  end

endmodule
