// Dcache Access Generator
// Aligns LD and ST instruction mask and data input to little endian bus

`define F3_LB   3'b000
`define F3_LH   3'b001
`define F3_LW   3'b010
`define F3_LBU  3'b100
`define F3_LHU  3'b101

`define F3_SB   3'b000
`define F3_SH   3'b001
`define F3_SW   3'b010

module dcache_acc_gen (data_in, ld_op, st_op, ls_type, addr_align_in,
                       mask_out, data_out);

  input [31:0] data_in;
  input ld_op, st_op;       // Currently not used
  input [2:0] ls_type;      // F3 input (B/H/W)
  input [1:0] addr_align_in;

  // Determines which byte(s) are being loaded or stored (Valid)
  output reg [3:0] mask_out; 

  // Reordered Store data
  output reg [31:0] data_out;


  // Address valid lines decoder
  always @(*) begin

    // When LW/SW, all bytes are valid
    if (ls_type[1] == 1'b1)
      mask_out = 4'b1111;
    else
      if (ls_type[0] == 1'b0) // Control is (L/S)B
        case ( addr_align_in )    // Decoder for address lines
          2'b00:  mask_out = 4'b1000;
          2'b01:  mask_out = 4'b0100;
          2'b10:  mask_out = 4'b0010;
          2'b11:  mask_out = 4'b0001;
          default: mask_out = 4'b1000; // Default is not needed
        endcase
      else  // ls_type is (L/S)H
        case ( addr_align_in )    // Decoder for address lines
          2'b00:  mask_out = 4'b1100;
          2'b01:  mask_out = 4'b1100; // Invalid Halfword Address
          2'b10:  mask_out = 4'b0011;
          2'b11:  mask_out = 4'b0011; // Invalid Halfword Address
          default: mask_out = 4'b1100; // Default is not needed
        endcase

  end


  // Reorders Stores
  always @(*) begin

    // Low order bits selection
    case (addr_align_in)
      2'b00:  data_out[7:0] = data_in[31:24];
      2'b01:  data_out[7:0] = data_in[23:16];
      2'b10:  data_out[7:0] = data_in[15:8];
      2'b11:  data_out[7:0] = data_in[7:0];
      default: data_out[7:0] = data_in[31:24]; // Default is not needed
    endcase

    // Second lowest order bits selection
    case (addr_align_in)
      2'b00:  data_out[15:8] = data_in[23:16];
      2'b01:  data_out[15:8] = data_in[23:16]; // don't care (not aligned)
      2'b10:  data_out[15:8] = data_in[7:0];
      2'b11:  data_out[15:8] = data_in[7:0]; // don't care (not aligned)
      default: data_out[15:8] = data_in[23:16]; // Default is not needed
    endcase

    // Second highest order bits selection
    case (addr_align_in)
      2'b00:  data_out[23:16] = data_in[15:8];
      2'b01:  data_out[23:16] = data_in[7:0];
      2'b10:  data_out[23:16] = data_in[7:0]; // don't care (not aligned)
      2'b11:  data_out[23:16] = data_in[7:0]; // don't care (not aligned)
      default: data_out[23:16] = data_in[7:0]; // Default is not needed
    endcase

    // High order bits selection (Can remove mux in the future if non aligned
    // is not needed)
    case (addr_align_in)
      2'b00:  data_out[31:24] = data_in[7:0];
      2'b01:  data_out[31:24] = data_in[7:0]; // don't care (not aligned)
      2'b10:  data_out[31:24] = data_in[7:0]; // don't care (not aligned)
      2'b11:  data_out[31:24] = data_in[7:0]; // don't care (not aligned)
      default: data_out[31:24] = data_in[7:0]; // Default is not needed
    endcase

  end


endmodule
