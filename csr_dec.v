`define MEPC        12'h341
`define MCAUSE      12'h342
`define MTVAL       12'h343
`define MIP         12'h344
`define MTVEC       12'h305
`define MSTATUS     12'h300
`define MIE         12'h304
`define MIP         12'h344
`define MSCRATCH    12'h340

`define INST_CSR    31:20       // CSR field

// CSR selection decoder

module csr_dec (inst, csr_dest, csr_val);
  
  input [31:0] inst;

  output reg [2:0] csr_dest;
  output reg csr_val;


    always @(*) begin
        
        case (inst[`INST_CSR])
            `MEPC:      begin csr_dest = 3'b000; csr_val = 1'b1; end
            `MCAUSE:    begin csr_dest = 3'b001; csr_val = 1'b1; end
            `MTVAL:     begin csr_dest = 3'b010; csr_val = 1'b1; end
            `MTVEC:     begin csr_dest = 3'b011; csr_val = 1'b1; end
            `MSTATUS:   begin csr_dest = 3'b100; csr_val = 1'b1; end
            `MIP:       begin csr_dest = 3'b101; csr_val = 1'b1; end
            `MIE:       begin csr_dest = 3'b110; csr_val = 1'b1; end
            `MSCRATCH:  begin csr_dest = 3'b111; csr_val = 1'b1; end
            default:    begin csr_dest = 3'b000; csr_val = 1'b0; end
        endcase

    end


endmodule
