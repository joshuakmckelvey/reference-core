PROG=user
TESTB=kirv_soc_tb
MARCH= -march=rv32g
COPS = -static -nostdlib -nostartfiles -ffreestanding -s -Qn -S

all : $(TESTB).vvp $(TESTB).vcd

$(TESTB).vvp : program
	iverilog -Wall -o $(TESTB).vvp -c modules.txt

$(TESTB).vcd : $(TESTB).vvp
	vvp $(TESTB).vvp

# Program is loaded to L2 cache
program : $(PROG).x
	elfconv --infile tests/$(PROG).x --split 0 --stride 1 --wordlen 4 --tagbits 18 --outfile l2_init
	touch mem_0.dat mem_1.dat mem_2.dat mem_3.dat
	touch text_0.dat text_1.dat text_2.dat text_3.dat
	touch data_0.dat data_1.dat data_2.dat data_3.dat
	bash ./l1_clr_dat.sh
	cp *.dat de0_modules/
	cp *.dat de2_modules/

# Program is loaded to Mem
#program : $(PROG).x
#	elfconv --infile tests/$(PROG).x --split 0 --stride 4 --wordlen 1 --tagbits 18 --outfile mem

# Program is generated from asm
#$(PROG).x : $(PROG).o
	#riscv32-unknown-linux-gnu-ld -T tests/linker.ld tests/$(PROG).o -o tests/$(PROG).x

#$(PROG).o : tests/$(PROG).asm
	#riscv32-unknown-linux-gnu-as tests/$(PROG).asm -o tests/$(PROG).o

# Program is generated from C
$(PROG).x : $(PROG).o tests/linker_user.ld
	riscv32-unknown-linux-gnu-ld -T tests/linker_user.ld tests/$(PROG).o -o tests/$(PROG).x

$(PROG).o : $(PROG).s tests/system.asm tests/syscall.h
	riscv32-unknown-linux-gnu-as tests/system.asm tests/$(PROG).s tests/syscall.asm -o tests/$(PROG).o

$(PROG).s : tests/$(PROG).c
	riscv32-unknown-linux-gnu-gcc $(MARCH) $(COPS) tests/$(PROG).c -o tests/$(PROG).s

clean :
	rm -f *.vvp *.vcd tests/*.o tests/*.s tests/*.x *.dat de0_modules/*.dat de2_modules/*.dat
