// Testing module for kirv_u

`define ZERO 32'd0

module kirv_tb ();
    
  reg clk;

  wire interrupt;
  
  // Processor to bus connections
  wire [31:0] cpu_in, cpu_out, cpu_addr;
  wire [3:0] cpu_addr_val;
  wire rd_wr;

  // Address space decoded lines
  reg mem_en, io_en, fb_en, lcd_data_en, lcd_ctrl_en, timer_en, rled_reg_en;

  // Inputs to cpu
  wire [31:0] mem_out, timer_out, fb_out;

  // lcd registers
  reg [7:0] lcd_data, lcd_ctrl;

  wire [31:0] vga_data_in;
  wire [31:0] vga_addr;

 
  kirv CPU        (.clk_i     (clk),
                   .int_i     ({1'b0, interrupt}),
                   .data_i    (cpu_in),
                   .data_o    (cpu_out),
                   .addr_o    (cpu_addr),
                   .addr_val_o(cpu_addr_val),
                   .data_dir_o(rd_wr));

  mem          #(.FILE      ("text"),
                 .ADDR_WIDTH(13))           // Currently 8KB
         MEM    (.clk_i     (clk),
                 .en_i      (mem_en),
                 .data_i    (cpu_out),
                 .addr_i    (cpu_addr),
                 .dir_i     (rd_wr),
                 .val_i     (cpu_addr_val),
                 .data_o    (mem_out));
					  
  // VGA Controller
  vga_ctrl VGA  (.vga_clk   (clk),
                 .data_in   (vga_data_in),
                 .vga_addr  (vga_addr),
                 .r_out     (),
                 .g_out     (),
                 .b_out     (),
                 .h_sync    (),
                 .v_sync    (),
                 .blank     (),
                 .sync      ());
					  
  fb_mem       #(.FILE      ("pic3"),
                 .ADDR_WIDTH(14))           // Currently 16KB
         FB     (.clk_i     (clk),
                 .en_i      (fb_en),
                 .data_i    (cpu_out),
                 .addr_i_0  (cpu_addr),
                 .addr_i_1  (vga_addr),
                 .dir_i     (rd_wr),
                 .val_i     (cpu_addr_val),
                 .data_o_0  (fb_out),       // Currently not connected
                 .data_o_1  (vga_data_in));

  // CPU input mux
  mux2_1x32 CPUMUX(.a(mem_out),
                   .b(timer_out),
                   .sel(io_en),
                   .out(cpu_in));

  timer TIMER     (.clk(!clk),    // Works on negedge clock
                   .din({cpu_out[7:0], cpu_out[15:8],
                         cpu_out[23:16], cpu_out[31:24]}),
                   .rd_wr(rd_wr),
                   .cur_cmp(cpu_addr[3]),
                   .low_high(cpu_addr[2]),
                   .ce(timer_en),
                   .dout({timer_out[7:0], timer_out[15:8],
                          timer_out[23:16], timer_out[31:24]}),
                   .int_out(interrupt));

  // Address Decoder
  always @(*) begin
    
    if (cpu_addr[31] == 0) begin
      mem_en <= 1;
      io_en <= 0;
      fb_en <= 0;
      lcd_data_en <= 1'b0;
      lcd_ctrl_en <= 1'b0;
      timer_en <= 1'b0;
      rled_reg_en <= 1'b0;
    end else begin

      mem_en <= 1'b0;

      if (cpu_addr[30] == 0) begin
      io_en <= 1'b1;
      fb_en <= 1'b0;

          if (cpu_addr[15:12] == 4'h0) begin
            if (!cpu_addr[3]) begin
              if (cpu_addr_val[3])	// LCD Data reg
                lcd_data_en <= 1'b1;
              else
                lcd_data_en <= 1'b0;
                 
              if (cpu_addr_val[2])	// LCD Control reg
                lcd_ctrl_en <= 1'b1;
              else
                lcd_ctrl_en <= 1'b0;

              rled_reg_en <= 1'b0;
            end else begin
              rled_reg_en <= 1'b1;
              lcd_ctrl_en <= 1'b0;
              lcd_data_en <= 1'b0;
            end

            timer_en <= 1'b0;
          end else if (cpu_addr[15:12] == 4'h1) begin
            lcd_data_en <= 1'b0;
            lcd_ctrl_en <= 1'b0;
            timer_en <= 1'b1;
            rled_reg_en <= 1'b0;
          end else begin

            lcd_data_en <= 1'b0;
            lcd_ctrl_en <= 1'b0;
            timer_en <= 1'b0;
            rled_reg_en <= 1'b0;

          end
      end else begin

        io_en <= 1'b0;
        fb_en <= 1'b1;

        lcd_data_en <= 1'b0;
        lcd_ctrl_en <= 1'b0;
        timer_en <= 1'b0;
        rled_reg_en <= 1'b0;

      end

    end
    
  end


  // lcd data register
  always @(negedge clk)
    if (lcd_data_en)
      lcd_data = cpu_out[31:24];
    
  // lcd control register
  always @(negedge clk)
    if (lcd_ctrl_en)
      lcd_ctrl = cpu_out[23:16];

  integer j;

  initial begin
      
      $dumpfile ("kirv_tb.vcd");
      $dumpvars (0, kirv_tb);
      
      /*
      for(j=1; j < 32; j++) begin
          $dumpvars(1, CPU.RF.rf[j]);
      end
      */

      $dumpvars(1, CPU.DATAPATH.RF.rf[1]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[2]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[3]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[4]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[5]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[6]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[7]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[8]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[9]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[10]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[11]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[12]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[13]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[14]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[15]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[16]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[17]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[18]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[19]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[20]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[21]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[22]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[23]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[24]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[25]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[26]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[27]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[28]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[29]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[30]);
      $dumpvars(1, CPU.DATAPATH.RF.rf[31]);


      CPU.DATAPATH.RF.rf[1] = `ZERO;
      CPU.DATAPATH.RF.rf[2] = `ZERO;
      CPU.DATAPATH.RF.rf[3] = `ZERO;
      CPU.DATAPATH.RF.rf[4] = `ZERO;
      CPU.DATAPATH.RF.rf[5] = `ZERO;
      CPU.DATAPATH.RF.rf[6] = `ZERO;
      CPU.DATAPATH.RF.rf[7] = `ZERO;
      CPU.DATAPATH.RF.rf[8] = `ZERO;
      CPU.DATAPATH.RF.rf[9] = `ZERO;
      CPU.DATAPATH.RF.rf[10] = `ZERO;
      CPU.DATAPATH.RF.rf[11] = `ZERO;
      CPU.DATAPATH.RF.rf[12] = `ZERO;
      CPU.DATAPATH.RF.rf[13] = `ZERO;
      CPU.DATAPATH.RF.rf[14] = `ZERO;
      CPU.DATAPATH.RF.rf[15] = `ZERO;
      CPU.DATAPATH.RF.rf[16] = `ZERO;
      CPU.DATAPATH.RF.rf[17] = `ZERO;
      CPU.DATAPATH.RF.rf[18] = `ZERO;
      CPU.DATAPATH.RF.rf[19] = `ZERO;
      CPU.DATAPATH.RF.rf[20] = `ZERO;
      CPU.DATAPATH.RF.rf[21] = `ZERO;
      CPU.DATAPATH.RF.rf[22] = `ZERO;
      CPU.DATAPATH.RF.rf[23] = `ZERO;
      CPU.DATAPATH.RF.rf[24] = `ZERO;
      CPU.DATAPATH.RF.rf[25] = `ZERO;
      CPU.DATAPATH.RF.rf[26] = `ZERO;
      CPU.DATAPATH.RF.rf[27] = `ZERO;
      CPU.DATAPATH.RF.rf[28] = `ZERO;
      CPU.DATAPATH.RF.rf[29] = `ZERO;
      CPU.DATAPATH.RF.rf[30] = `ZERO;
      CPU.DATAPATH.RF.rf[31] = `ZERO;

      /*
      for (j=0; j<1024; j=j+1) begin // Clears Memory
          MEM.mem0[j] = 8'h0;
          MEM.mem1[j] = 8'h0;
          MEM.mem2[j] = 8'h0;
          MEM.mem3[j] = 8'h0;
      end
      */

      // Reads into memory
      #1 
      /*
      $readmemh("./tests/test4_0.dat", MEM.mem0);
      $readmemh("./tests/test4_1.dat", MEM.mem1);
      $readmemh("./tests/test4_2.dat", MEM.mem2);
      $readmemh("./tests/test4_3.dat", MEM.mem3);
      */

      /*
      clr = 1'b0; #8 clr = 1'b1; #10 clr = 1'b0;
      */

      /*
      for (j=0; j<32; j=j+1)
          $display("Mem at: %d, %h", j, MEM.mem[j]);
      */

      #5000 $finish;
  end

  always begin
      
      #5 clk = 1'b0;
      #5 clk = 1'b1;

      /*
      $display("Register Contents:");
      //$display("  r0 - %h", CPU.RF.rf[0]);
      $display("  r1 - %h", CPU.RF.rf[1]);
      $display("  r2 - %h", CPU.RF.rf[2]);
      $display("  r3 - %h", CPU.RF.rf[3]);
      $display("  r4 - %h", CPU.RF.rf[4]);
      $display("  r5 - %h", CPU.RF.rf[5]);
      $display("  r6 - %h", CPU.RF.rf[6]);
      $display("  r7 - %h", CPU.RF.rf[7]);
      */

  end

endmodule
