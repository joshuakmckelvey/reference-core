// DE2 interface for kirv processor
module de2 (

  input CLOCK_50,
  input [17:0] SW,
  input [3:0] KEY,
  input UART_RXD,
  output [17:0] LEDR,
  output [8:0] LEDG,
  output [6:0] HEX0, HEX1, HEX2, HEX3, HEX4, HEX5, HEX6, HEX7,

  output [7:0] LCD_DATA,
  output LCD_RW, LCD_EN, LCD_RS, LCD_ON, LCD_BLON,
  output UART_TXD

);

  wire single_step;

  // lcd registers
  wire [7:0] lcd_data, lcd_ctrl;
   
  wire [31:0] hex_disp;

  wire [31:0] rled_reg;

  kirv_soc SYSTEM (.CLOCK_50  (CLOCK_50),
                   .switch_i  (SW[15:0]),
                   .key_i     (KEY),
                   .uart_rxd  (UART_RXD),
                   .hex_disp  (hex_disp),
                   .rled_reg_o(rled_reg),
                   .uart_txd  (UART_TXD),
                   .lcd_data_o(lcd_data),
                   .lcd_ctrl_o(lcd_ctrl));

    
/* NOTE: Bring PLL up from soc?
  // Clock PLL (50MHz to 10MHz)
  pll_50m_10m PLL (.inclk0    (CLOCK_50),
                   .c0        (clk));
*/

/*
  // 32 bit clock divider (28'h2000000 gives 1 hz)
  // 28'h200 gives 700 khz
  // 28'h400000 gives fast manual
 
  clk_gen      #(.DIV       (28'h2000)) 
        CLK_GEN (.clk_in    (CLOCK_50),
                 .ss_in     (single_step), // Single step
                 .sel       (SW[0]),
                 .clk_out   (clk));
*/

  // Hex display
  hex_disp DISP0	(.in(hex_disp[3:0]),
                     .ca(HEX0[6:0]));
             
  hex_disp DISP1	(.in(hex_disp[7:4]),
                     .ca(HEX1[6:0]));

  hex_disp DISP2	(.in(hex_disp[11:8]),
                     .ca(HEX2[6:0]));
             
  hex_disp DISP3	(.in(hex_disp[15:12]),
                     .ca(HEX3[6:0]));

  hex_disp DISP4	(.in(hex_disp[19:16]),
                     .ca(HEX4[6:0]));
             
  hex_disp DISP5	(.in(hex_disp[23:20]),
                     .ca(HEX5[6:0]));
             
  hex_disp DISP6	(.in(hex_disp[27:24]),
                     .ca(HEX6[6:0]));
             
  hex_disp DISP7	(.in(hex_disp[31:28]),
                     .ca(HEX7[6:0]));
  
  //assign in_sw = SW[15:0];

  //assign interrupt = !KEY[3];

  assign single_step = !KEY[0];

  assign LEDR[15:0] = SW[17] ? rled_reg[31:16] : rled_reg[15:0];

  assign LEDR[17:16] = 2'b0;

  assign LEDG[8:1] = 8'b0;

  assign LEDG[0] = 1'b1;

  // LCD Controls
  assign LCD_DATA[7:0] = lcd_data[7:0];
  assign LCD_RW = lcd_ctrl[5];
  assign LCD_EN = lcd_ctrl[7];
  assign LCD_RS = lcd_ctrl[6];
  assign LCD_ON = 1'b1;
  assign LCD_BLON = 1'b0;
	
endmodule
