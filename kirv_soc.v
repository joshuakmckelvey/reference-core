// KIRV System
module kirv_soc (

  input CLOCK_50,
  input [15:0] switch_i,
  input [3:0] key_i,
  input uart_rxd,
  output [31:0] rled_reg_o,
  output [31:0] hex_disp,

  output [7:0] lcd_data_o,
  output [7:0] lcd_ctrl_o,
  output uart_txd

);
   
  wire clk;

  // Address space decoded lines
  reg lcd_data_en, lcd_ctrl_en, timer_en, uart_en, rled_reg_en;

  // lcd registers
  reg [7:0] lcd_data, lcd_ctrl;
  
  // RLED register
  reg [31:0] rled_reg;

  wire [31:0] pc_out_t;

  // Memory interface
  wire [31:0] mem_in, mem_out, mem_addr;
  wire mem_en, mem_we;
  wire mem_val_out;

  // Peripheral interface
  wire [31:0] per_out, per_addr;
  wire per_en, per_we;
  wire [3:0] per_be;  // Byte enable
  reg [31:0] per_data_in;
  reg per_val_out;

  // Timer wires
  wire [31:0] timer_data_out;
  wire timer_val_out;
  wire timer_int;

  // UART wires
  wire [31:0] uart_data_out;
  wire uart_val_out;
  wire uart_int;

  kirv_core CPU (.clk_i     (clk),
                 .int_i     ({uart_int, timer_int}),
                 .mem_data_i(mem_in),
                 .mem_val_i (mem_val_out),
                 .mem_data_o(mem_out),
                 .mem_addr_o(mem_addr),
                 .mem_en_o  (mem_en),
                 .mem_we_o  (mem_we),
                 .per_data_i(per_data_in),
                 .per_val_i (per_val_out),
                 .per_data_o(per_out),
                 .per_addr_o(per_addr),
                 .per_en_o  (per_en),
                 .per_we_o  (per_we),
                 .per_be_o  (per_be),       // Byte enable
                 .pc_out_test (pc_out_t));  // For Debugging

  generic_mem  #(.FILE      ("mem"))
         MEM    (.clk_i     (clk),
                 .en_i      (mem_en),
                 .data_i    (mem_out),
                 .addr_i    (mem_addr), // Currently 8KB
                 .dir_i     (mem_we),
                 .val_o     (mem_val_out),
                 .data_o    (mem_in));

  // Clock PLL (50MHz to 10MHz)
  pll_50m_10m PLL (.inclk0    (CLOCK_50),
                   .c0        (clk));
  
  timer TIMER       (.clk_i     (clk),
                     .data_i    (per_out),
                     .rd_wr_i   (per_we),
                     .cur_cmp_i (per_addr[3]),
                     .low_high_i(per_addr[2]),
                     .ce_i      (timer_en & per_en),
                     .data_o    (timer_data_out),
                     .val_o     (timer_val_out),
                     .int_o     (timer_int));

  // RS-232 UART: 8 bit, no parity
  uart             #(.REF_CLK   (10000000),
                     .BAUDRATE  (38400))
       UART         (.clk_i     (clk),
                     .rxd_i     (uart_rxd),
                     .ce_i      (uart_en & per_en),
                     .func_i    (per_addr[2]),
                     .data_i    (per_out),
                     .data_o    (uart_data_out),
                     .val_o     (uart_val_out),
                     .int_o     (uart_int),
                     .txd_o     (uart_txd));

  // Address Decoder
  always @(*) begin
    
    lcd_data_en <= 1'b0;
    lcd_ctrl_en <= 1'b0;
    timer_en    <= 1'b0;
    uart_en     <= 1'b0;
    rled_reg_en <= 1'b0;
    per_val_out <= 1'b0;
    per_data_in <= 32'b0;


      if (per_addr[31] == 0) begin
        lcd_data_en <= 1'b0;
        lcd_ctrl_en <= 1'b0;
        timer_en    <= 1'b0;
        uart_en     <= 1'b0;
        rled_reg_en <= 1'b0;
      end else begin

        if (per_addr[15:12] == 4'h0) begin
          if (!per_addr[3]) begin
            if (per_be[3])	// LCD Data reg
              lcd_data_en <= 1'b1;
            if (per_be[2])	// LCD Control reg
              lcd_ctrl_en <= 1'b1;
          end else begin
            rled_reg_en <= 1'b1;
          end

        end else if (per_addr[15:12] == 4'h1) begin

          timer_en    <= 1'b1;
          per_val_out <= timer_val_out;
          per_data_in <= timer_data_out;

        end else if (per_addr[15:12] == 4'h3) begin

          uart_en     <= 1'b1;
          per_val_out <= uart_val_out;
          per_data_in <= uart_data_out;

        end else begin
          
          // Invalid Peripheral Address
          per_val_out <= 1'b0;

        end

      end
    
  end

  // lcd data register
  always @(posedge clk)
    if (lcd_data_en & per_en)
      lcd_data <= per_out[31:24];
    
  // lcd control register
  always @(posedge clk)
    if (lcd_ctrl_en & per_en)
      lcd_ctrl <= per_out[23:16];

  // Red led output register
  always @(posedge clk)
    if (rled_reg_en && per_en && (per_be[0] || per_be[1] || per_be[2] || per_be[3])) begin
      rled_reg[7:0] <= per_out[31:24];
      rled_reg[15:8] <= per_out[23:16];
      rled_reg[23:16] <= per_out[15:8];
      rled_reg[31:24] <= per_out[7:0];
    end

  assign lcd_data_o = lcd_data;
  assign lcd_ctrl_o = lcd_ctrl;

  assign rled_reg_o = rled_reg;

  assign hex_disp = pc_out_t;
	
endmodule
