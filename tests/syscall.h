#ifndef SYSCALL_H
#define SYSCALL_H

// Assembly functions and syscalls
extern int myadd(int a, int b);
extern int get_time();
extern char getchar_uart();
extern void puts_lcd(char* a);
extern void puts_uart(char* a);
extern void puts_led(char* a);
extern void init_lcd();
extern void _exit();

#endif
