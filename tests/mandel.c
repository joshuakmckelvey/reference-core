/* Prints the Mandelbrot set on UART */

#include "syscall.h"

#define FRAC_BITS 16

#define HEIGHT    64
#define HEIGHT_S  6
#define WIDTH     128
#define WIDTH_S   7

#define MAX_ITER  11

#define FP(x) ( (x) << FRAC_BITS )

char* inittxt = "\r\n\nINIT\r\n";

// Prints char to UART
void printchar (char a) {
  
  char message3[2];

  message3[0] = a;
  message3[1] = '\0';

  puts_uart(message3);

}

// Prints integer in hex if between 0-F
void printchar_hex (int a) {
  
  char message3[8];

  // Print A-F
  if (a > 9) {

    message3[0] = (char) (a + 55);

  // Print 0-9
  } else {

    message3[0] = (char) (a + 48);

  }

  message3[1] = '\0';
  puts_uart(message3);


}

// Recursive Helper
void printint_hex_rec (int a) {
  
  if (a > 0) {

    printint_hex_rec(a >> 4);
    printchar_hex(a & 0xF);

  }

}

// Prints an (unsigned) integer in hex
void printint_hex (int a) {
  
  printint_hex_rec(a >> 4);
  printchar_hex(a & 0xF);

}

// Prints a decimal integer to UART, 0-9
void printint (int a) {

  char message3[8];

  /* If a is positive */
  if (a > 0) {

    message3[0] = (char) (a + 48);
    message3[1] = '\0';

  }

  puts_uart(message3);

}

unsigned int umult(unsigned int n, unsigned int m) {

  unsigned int total = 0;

  while (m) {

    if (m & 1) {
      
      total = total + n;

    }

    n = n << 1;
    m = m >> 1;

  }

  return total;

}

int mult(int n, int m) {

  int sign = (n & (1<<31)) ^ (m & (1<<31));

  if (n < 0) {
    n = -n;
  }
  if (m < 0) {
    m = -m;
  }

  int total = 0;

  while (m) {

    if (m & 1) {
      
      total = total + n;

    }

    n = n << 1;
    m = m >> 1;

  }

  if (sign) {
    
    total = -total;

  }

  return total;

}

int fp_mult(int a, int b) {

  a = a >> (FRAC_BITS/2);
  b = b >> (FRAC_BITS/2);

  return mult(a,b);

}

void mandel () {

  int w_scale = FP(1);
  int w_trans = FP(-2);
  int h_scale = FP(1);
  int h_trans = FP(-1)>>1;
  
  for (int row = 0; row < HEIGHT; row++) {

    for (int col = 0; col < WIDTH; col++) {
      
      int c_re = (fp_mult(FP(col) + fp_mult(FP(WIDTH), w_trans), w_scale)) >> WIDTH_S;
      int c_im = (fp_mult(FP(row) + fp_mult(FP(HEIGHT), h_trans), h_scale)) >> WIDTH_S;
      int x = 0;
      int y = 0;

      int iter = 0;

      while ((fp_mult(x,x) + fp_mult(y,y) <= FP(4)) && iter < MAX_ITER) {
        
        int x_next = fp_mult(x,x) - fp_mult(y,y) + c_re;

        y = (fp_mult(x,y)<<1) + c_im;

        x = x_next;

        iter++;

      }

      if (iter == 0) {
        
        puts_uart("0");

      } else if (iter == 1) {
        
        puts_uart(" ");

      } else if (iter == 2) {
        
        puts_uart(".");

      } else if (iter == 3) {
        
        puts_uart(":");

      } else if (iter == 4) {
        
        puts_uart("-");

      } else if (iter == 5) {
        
        puts_uart("+");

      } else if (iter == 6) {
        
        puts_uart("*");

      } else if (iter == 7) {

        puts_uart("x");

      } else if (iter == 8) {
        
        puts_uart("X");

      } else if (iter == 9) {
        
        puts_uart("#");

      } else if (iter == 10) {
        
        puts_uart("$");

      } else if (iter == 11) {
        
        puts_uart("@");

      }

    }

    puts_uart("\r\n");

  }

}

// Main program
void _start () {

  puts_uart("Mandelbrot Set:\r\n");
  mandel();

/*
  puts_uart("Window Size: [");
  printint_hex(x0);
  puts_uart(", ");
  printint_hex(y0);
  puts_uart("] [");
  printint_hex(x1);
  puts_uart(", ");
  printint_hex(y1);
  puts_uart("]");
*/
  puts_uart("\r\n");

  // UART Loopback
  while (1) {
    printchar(getchar_uart());
  }

  puts_uart("ERROR: Should Not Reach!\r\n");
  
  _exit();

}
