/* Tests printing to lcd and uart from user C code */

#include "syscall.h"

char* inittxt = "\r\n\nINIT\r\n";
char* message0 = "Hallo GCC!\n";
char* message1 = "Hallo UART!\r\n";
char message2[] = "----\r\n";

// Prints integer in hex if between 0-F
void printchar_hex (int a) {
  
  char message3[8];

  // Print A-F
  if (a > 9) {

    message3[0] = (char) (a + 55);

  // Print 0-9
  } else {

    message3[0] = (char) (a + 48);

  }

  message3[1] = '\0';
  puts_uart(message3);


}

// Recursive Helper
void printint_hex_rec (int a) {
  
  if (a > 0) {

    printint_hex_rec(a >> 4);
    printchar_hex(a & 0xF);

  }

}

// Prints an (unsigned) integer in hex
void printint_hex (int a) {
  
  printint_hex_rec(a >> 4);
  printchar_hex(a & 0xF);

}

// Prints an (unsigned) integer in hex-8 chars
void printint_hex_pad (int a) {
  
  for (int i = 0; i < 8; i++) {
    printchar_hex((char) ((a >> (28-4*i)) & 0xF));
  }

}

// Prints a decimal integer to UART, 0-9
void printint (int a) {

  char message3[8];

  /* If a is positive */
  if (a > 0) {

    message3[0] = (char) (a + 48);
    message3[1] = '\0';

  }

  puts_uart(message3);

}

// Summation series
int recsum(int a) {
  
  if (a > 0) {
    
    return a + recsum(a-1);

  } else {

    return 0;

  }

}

// Dumps Memory From start to end-1
void dump_mem (int* start, int* end) {

  for(int* i = start; i < end; i++) {

    printint_hex_pad(*i);

    // Prints newline every 4 words
    if ((((int) i) & 0xC) == 0xC) {
      puts_uart("\r\n");
    } else {
      puts_uart(" ");
    }

  }

}

// Main program
void _start () {

  //init_lcd();
  //puts_lcd(message0);

  puts_uart(inittxt);

  puts_uart("Mem bytes 0-255:");
  puts_uart("\r\n");

  dump_mem((int*) 0, (int*) 256);

  puts_uart("\r\n");
  
  _exit();

}
