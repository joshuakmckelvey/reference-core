	.option nopic
    .data
    .section	.rodata
	.align	2
.lcd_init_message:
	.string	"Starting Program\r\n"
.lcd_exit_message:
	.string	"Finishing Program\r\n"

	.text
	.align	2
	#.globl	main
    .globl _start
	.globl	_main
	#.type	main, @function
	.type	_start, @function

    # Use for 100MHz
    #.set    lcd_delay, 2048
    #.set    long_delay, 512

    # Use for Slow Clock (<10MHz)
    .set    lcd_delay, 512
    .set    long_delay, 128

    # Use for Fast Manual Clock (<700MHz)
    #.set    lcd_delay, 128
    #.set    long_delay, 1

    # Use for Slow Manual Clock
    #.set    lcd_delay, 16
    #.set    long_delay, 1


_main: 			# Initialize CSRs, SP, call user level program
    li sp, 0x00002000 # Initialize stack pointer

	la t0, tsr	# mtvec offset
	csrw mtvec, t0	# Sets mtvec to tsr address
	
	li t0, 0x110
    li t1, 0x80001000
	sw t0, 0x0(t1)	    # Sets mtime_cmp to 272 cycles
	sw zero, 0x4(t1)	# mtime_cmp high

    #li t0, 0x880           # External and Timer interrupt mask
	#csrrs  zero, mie, t0    # Enables Timer and External Machine interrupts
    li t0, 0x880            # External and timer interrupt mask
	csrc  mie, t0           # Disables External and timer interrupts
	csrsi mstatus, 0x8	# Enables Interrupts (MIE) in mstatus
    #li t0, 0x800            # External interrupt mask
	#csrrs  zero, mie, t0    # Enables External interrupts

	la t0, _start	# Loads addr of user start
	csrw 0x341, t0	# Sets mepc to user start
    mret


tsr:			# Interrupt/exception service routine
    addi sp, sp, -40 # Push registers onto stack
    sw t0, 28(sp)
    sw t1, 24(sp)
    sw t2, 20(sp)
    sw t3, 16(sp)
    sw t4, 12(sp)
    sw t5, 8(sp)
    sw a0, 4(sp)
    sw a1, 0(sp)
	csrr t0, mstatus	# Read mcause CSR
    sw t0, 32(sp)
	csrr t0, 0x341	# Read mepc CSR
    sw t0, 36(sp)
    

	csrr t0, 0x342	# Read mcause CSR
	#srli t1, t0, 31 # Extract Interrupt/Exception bit
	#bnez  t1, isr
	li t1, 0x80000007	# Machine timer interrupt
	beq t0, t1, timer_isr
	li t1, 0x8000000B	# Machine external interrupt
	beq t0, t1, ext_isr
	li t1, 0x8	# ecall from user cause
	beq t0, t1, ecall_user
	li t1, 0xB	# ecall from machine cause
	beq t0, t1, ecall_machine
    # Output mepc to led
    li t2, 0x80000008
	csrr t1, 0x341	# Read mepc CSR
	sw t1, 0(t2)	# Store shifted mcause into Output LEDs
	li t1, 0x2	# Illegal instruction cause
	beq t0, t1, ill_inst
	j inv_except	# Invalid Exception

ecall_user:
	li t0, 4
	beq a0, t0, print_s_lcd # branches if print_s_lcd syscall
	li t0, 5
	beq a0, t0, print_s_uart # branches if print_s_uart syscall
	li t0, 6
	beq a0, t0, print_s_led # branches if print_s_led syscall
	li t0, 9
	beq a0, t0, lcd_init    # lcd_init syscall
	li t0, 12
	beq a0, t0, getchar_sys    # getchar_sys syscall
	li t0, 10
	beq a0, t0, exit    # branches if exit syscall
	j inv_call	# Invalid syscall

ecall_machine:
	li t0, 4
	beq a0, t0, print_s_lcd # branches if print_s_lcd syscall
	li t0, 5
	beq a0, t0, print_s_uart # branches if print_s_uart syscall
	li t0, 6
	beq a0, t0, print_s_led # branches if print_s_led syscall
	li t0, 8
	beq a0, t0, set_timer # branches if set_timer syscall
	li t0, 9
	beq a0, t0, wait_sys # branches if wait syscall
	li t0, 10
	beq a0, t0, exit    # branches if exit syscall
	j inv_call	# Invalid syscall

timer_isr:
    li t0, 0x080
	csrc  mie, t0       # Disables Timer interrupt
    j int_ret

# Currently only connected to UART
ext_isr:
    li t0, 0x800
	csrc  mie, t0       # Disables External interrupt

	li t3, 0x80003004   # UART RX register location
    lb t1, 3(t3)        # Load char from UART RX Buffer

	li t3, 0x80003000   # UART TX register location
    sb t1, 3(t3)        # Store char to UART TX Buffer

    li t0, 0x800
	csrrs  zero, mie, t0    # Enables External interrupts
    j int_ret

exit:   # Waits for interrupt
    li t0, 0x800            # External interrupt mask
	csrrs  zero, mie, t0    # Enables External interrupts
	csrsi mstatus, 0x8	# Enables Interrupts (MIE) in mstatus
infloop:
    j infloop
    

ill_inst: 		# Illegal instruction occured
	j ill_inst	# Infinite Loop

inv_call: 		# Invalid syscall occured
	j inv_call	# Infinite Loop

inv_except: 		# Invalid interrupt occured
	j inv_except	# Infinite Loop	

inv_interrupt: 		# Invalid interrupt occured
	j inv_interrupt	# Infinite Loop

# Sets timer interrupt to occur in a1 cycles
set_timer:              # Sets timer to a1 cycles from now
    li t4, 0x80001000   # Base addr of mtime
	lw t0, 0x0008(t4)	# Loads mtime low
	lw t1, 0x000C(t4)	# Loads mtime high
	lw t2, 0x0000(t4)	# Loads mtime_cmp low
	lw t3, 0x0004(t4)	# Loads mtime_cmp high
	add t0, t0, a1	    # adds a1 cycles to last time count
	sw t0, 0x0000(t4)	# Sets mtime_cmp
    li t0, 0x080
	csrrs  zero, mie, t0    # Enables Timer interrupt
    j exc_ret

# Waits for a1 cycles (timer interrupt)
wait_sys:
	csrci mstatus, 0x8	# Disables Interrupts (MIE) in mstatus
    li t4, 0x80001000   # Base addr of mtime
	lw t0, 0x0008(t4)	# Loads mtime low
	lw t1, 0x000C(t4)	# Loads mtime high
	lw t2, 0x0000(t4)	# Loads mtime_cmp low
	lw t3, 0x0004(t4)	# Loads mtime_cmp high
	add t0, t0, a1	    # adds a1 cycles to last time count
	sw t0, 0x0000(t4)	# Sets mtime_cmp
    addi zero, zero, 0
    addi zero, zero, 0
    addi zero, zero, 0
    addi zero, zero, 0
idle:
	csrr t0, mip
    andi t0, t0, 0x080
	beq t0, zero, idle   # Loops while timer interrupts has not occured
	csrsi mstatus, 0x8	# Enables Interrupts (MIE) in mstatus
    j exc_ret

# Prints null terminated string to LCD
print_s_lcd:
	addi t1, a1, 0 	# Copy address of string
	li t2, 0x80000000 # LCD Control/Data register location
	li t3, 0xc0	# LCD Control set: char out, clk high
	li t4, 0x40	# LCD Control set: char out, clk low
	lb   t0, 0(t1)  # Load char
	sb   t4, 1(t2)  # Init LCD Control to clk low

	li a0, 9	# Loads wait_sys syscall
	li a1, lcd_delay   # Number of Cycles to wait
    ecall

loop_lcd:
	sb   t0, 0(t2)  # Store char in lcd data reg

	li a0, 9	# Loads wait_sys syscall
	li a1, lcd_delay   # Number of Cycles to wait
    ecall

	sb   t3, 1(t2)  # Toggle LCD clock

	li a0, 9	# Loads wait_sys syscall
	li a1, lcd_delay   # Number of Cycles to wait
    ecall

	sb   t4, 1(t2)

	li a0, 9	# Loads wait_sys syscall
	li a1, lcd_delay   # Number of Cycles to wait
    ecall

	addi t1, t1, 1  # Next char addr
	lb   t0, 0(t1)  # Load char
	bne  zero, t0, loop_lcd	# Repeat until NULL char

    j exc_ret
    
# Prints null terminated string to UART
print_s_uart:
	addi t1, a1, 0 	# Copy address of string
	lb   t0, 0(t1)  # Load char

	li t5, 0x80003000 # UART register location

loop_uart:
    sb t0, 3(t5)    # Store char in UART TX Buffer

	addi t1, t1, 1  # Next char addr
	lb   t0, 0(t1)  # Load char
	bne  zero, t0, loop_uart	# Repeat until NULL char

    j exc_ret

# Prints null terminated string to LED Register
print_s_led:
	addi t1, a1, 0 	# Copy address of string
	lb   t0, 0(t1)  # Load char

	li t5, 0x80000008 # LED register location

loop_led:
    sb t0, 3(t5)    # Store char in LED Buffer

	addi t1, t1, 1  # Next char addr
	lb   t0, 0(t1)  # Load char
	bne  zero, t0, loop_led	# Repeat until NULL char

    j exc_ret

# Waits for char in UART
getchar_sys:
    li t0, 0x800            # External interrupt mask

getchar_sys_idle:
	csrr t1, mip 	# Checks for UART RX interrupt
    and t1, t1, t0
	beq t1, zero, getchar_sys_idle   # Loops while uart interrupt has not occured

	li t1, 0x80003004   # UART RX register location
    lb a0, 3(t1)        # Load char from UART RX Buffer

    j syscall_ret

# Initialize LCD
lcd_init:

	li t2, 0x80000000 # LCD Control/Data register location
	li t4, 0x00	# LCD Control set: ctrl out, clk low

	sb   t4, 1(t2) 	# Init LCD Control to clk low

	li a0, 9	# Loads wait_sys syscall
	li a1, lcd_delay   # Number of Cycles to wait
    ecall

	li   t0, 0x8038 # Sets 8 bit 2 line display
	sh   t0, 0(t2) 	# Store char in lcd data reg, LCD clock high

	li a0, 9	# Loads wait_sys syscall
	li a1, lcd_delay   # Number of Cycles to wait
    ecall

	sb   t4, 1(t2) 	# LCD clock low

	li a0, 9	# Loads wait_sys syscall
	li a1, lcd_delay   # Number of Cycles to wait
    ecall

	li   t0, 0x800e # Turn on display/cursor
	sh   t0, 0(t2) 	# Store char in lcd data reg, LCD clock high

	li a0, 9	# Loads wait_sys syscall
	li a1, lcd_delay   # Number of Cycles to wait
    ecall

	sb   t4, 1(t2) 	# LCD clock low

	li a0, 9	# Loads wait_sys syscall
	li a1, lcd_delay   # Number of Cycles to wait
    ecall

	li   t0, 0x8006 # Mode:inc and shift cursor right
	sh   t0, 0(t2) 	# Store char in lcd data reg, LCD clock high

	li a0, 9	# Loads wait_sys syscall
	li a1, lcd_delay   # Number of Cycles to wait
    ecall

	sb   t4, 1(t2) 	# LCD clock low

	li a0, 9	# Loads wait_sys syscall
	li a1, lcd_delay   # Number of Cycles to wait
    ecall

	li   t0, 0x8001 # Clear Display (Takes ~2ms, requires longer delay)
	sh   t0, 0(t2) 	# Store char in lcd data reg, LCD clock high

	li a0, 9	# Loads wait_sys syscall
	li a1, lcd_delay*long_delay  # Number of Cycles to wait
    ecall

	sb   t4, 1(t2) 	# LCD clock low

	li a0, 9	# Loads wait_sys syscall
	li a1, lcd_delay*long_delay  # Number of Cycles to wait
    ecall

    # Return from syscall
    j exc_ret
    
# Return from interrupt
int_ret:
    lw t0, 36(sp)
	csrw mepc, t0	# Write mepc CSR
    lw t0, 32(sp)
	csrw mstatus, t0	# Write mcause CSR
    lw t0, 28(sp)   # Pop registers from stack
    lw t1, 24(sp)
    lw t2, 20(sp)
    lw t3, 16(sp)
    lw t4, 12(sp)
    lw t5, 8(sp)
    lw a0, 4(sp)
    lw a1, 0(sp)
    addi sp, sp, 40
    mret

# Return from exception
exc_ret:
    lw t0, 36(sp)
	addi t0, t0, 4	# Adds 1 instruction offset
	csrw mepc, t0	# Write mepc CSR
    lw t0, 32(sp)
	csrw mstatus, t0	# Write mcause CSR
    lw t0, 28(sp)   # Pop registers from stack
    lw t1, 24(sp)
    lw t2, 20(sp)
    lw t3, 16(sp)
    lw t4, 12(sp)
    lw t5, 8(sp)
    lw a0, 4(sp)
    lw a1, 0(sp)
    addi sp, sp, 40
    mret

# Return from syscall (with args)
syscall_ret:
    lw t0, 36(sp)
	addi t0, t0, 4	# Adds 1 instruction offset
	csrw mepc, t0	# Write mepc CSR
    lw t0, 32(sp)
	csrw mstatus, t0	# Write mcause CSR
    lw t0, 28(sp)   # Pop registers from stack
    lw t1, 24(sp)
    lw t2, 20(sp)
    lw t3, 16(sp)
    lw t4, 12(sp)
    lw t5, 8(sp)
    #lw a0, 4(sp)
    #lw a1, 0(sp)
    addi sp, sp, 40
    mret

/*
main:           # User level program

    # Print Starting message to LCD
	li a0, 4	# Loads print_s syscall code
	la a1, .lcd_init_message # Address of string
	ecall		# Call print_s_lcd


    # Print Ending message to LCD
	li a0, 4	# Loads print_s syscall code
	la a1, .lcd_exit_message # Address of string
	ecall		# Call print_s_lcd

	li a0, 10	# Loads exit syscall code
	ecall		# Call exit
	nop		# Padding
    */

	#.size	main, .-main
	.section	.note.GNU-stack,"",@progbits
