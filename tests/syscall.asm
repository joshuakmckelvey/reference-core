.option nopic
.text

# Functions
.align 2
.globl   myadd
.type    myadd, @function
myadd:
    add a0, a0, a1      # a0 is return value
    ret


.align 2
.globl   get_time
.type    get_time, @function
get_time:
    addi sp, sp, -4
    sw t0, 0(sp)

    li t0, 0x80001000   # Base addr of mtime
	lw a0, 0x0008(t0)	# Loads mtime low

    lw t0, 0(sp)
    addi sp, sp, 4
    ret                 # Return by branching to the address in the link register.


# System Call Wrappers
.align 2
.globl   puts_uart
.type    puts_uart, @function
puts_uart:
    addi sp, sp, -4
    sw ra, 0(sp)

    # Move addr arg to a1
    addi a1, a0, 0

    li a0, 5    # Loads print_s uart syscall code
    ecall

    lw ra, 0(sp)
    addi sp, sp, 4
    ret                 # Return by branching to the address in the link register.


.align 2
.globl   puts_lcd
.type    puts_lcd, @function
puts_lcd:
    addi sp, sp, -4
    sw ra, 0(sp)

    # Move addr arg to a1
    addi a1, a0, 0

    li a0, 4    # Loads print_s lcd syscall code
    ecall

    lw ra, 0(sp)
    addi sp, sp, 4
    ret                 # Return by branching to the address in the link register.


.align 2
.globl   puts_led
.type    puts_led, @function
puts_led:
    addi sp, sp, -4
    sw ra, 0(sp)

    # Move addr arg to a1
    addi a1, a0, 0

    li a0, 6    # Loads print_s led syscall code
    ecall

    lw ra, 0(sp)
    addi sp, sp, 4
    ret                 # Return by branching to the address in the link register.


.align 2
.globl   init_lcd
.type    init_lcd, @function
init_lcd:
    addi sp, sp, -4
    sw ra, 0(sp)

    li a0, 9    # Loads init lcd syscall code
    ecall

    lw ra, 0(sp)
    addi sp, sp, 4
    ret                 # Return by branching to the address in the link register.


.align 2
.globl   getchar_uart
.type    getchar_uart, @function
getchar_uart:
    addi sp, sp, -4
    sw ra, 0(sp)

    li a0, 12    # Loads getchar_sys syscall code
    ecall

    lw ra, 0(sp)
    addi sp, sp, 4
    ret                 # Return by branching to the address in the link register.


.align 2
.globl   _exit
.type    _exit, @function
_exit:
    li a0, 10    # Loads exit syscall code
    ecall
