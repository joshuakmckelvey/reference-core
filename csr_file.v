`define MEPC        3'b000
`define MCAUSE      3'b001
`define MTVAL       3'b010
`define MTVEC       3'b011
`define MSTATUS     3'b100
`define MIP         3'b101
`define MIE         3'b110
`define MSCRATCH    3'b111

`define ECAUSE_NONE   3'b000
`define ECAUSE_ECALL  3'b001
`define ECAUSE_MRET   3'b010
`define ECAUSE_MTIME  3'b100
`define ECAUSE_MEXT   3'b101
`define ECAUSE_INV    3'b111

`define MTIE    7   // Timer interrupt enabled bit
`define MEIE    11  // External interrupt enabled bit
`define MTIP    7   // Timer interrupt pending bit
`define MEIP    11  // External interrupt pending bit

  
// CSR File
module csr_file (clk_i, csr_addr_1_i, csr_addr_2_i, csr_data_2_i, wr_en_2_i,
                 ecause_i, exc_tak_i, ret_tak_i, nmepc_i, int_i,
                 priv_o, mtvec_o, mepc_o, mie_o, mstatus_o, csr_data_1_o);
  
  input clk_i;
  input [2:0] csr_addr_1_i, csr_addr_2_i;
  input [31:0] csr_data_2_i;
  input wr_en_2_i;
  input [2:0] ecause_i;
  input exc_tak_i, ret_tak_i;
  input [31:0] nmepc_i;
  input [1:0] int_i;

  output reg [1:0] priv_o;  // Current processor priv

  output [31:0] mtvec_o, mepc_o, mie_o, mstatus_o;
  output [31:0] csr_data_1_o;

  reg [31:0] mie_reg;   // MIE Register
  reg [31:0] mip_reg;   // MIP Register

  reg mepc_ld, mcause_ld, mtval_ld, mtvec_ld,
      mstatus_ld, mip_ld, mie_ld, mscratch_ld;

  reg [31:0] next_mcause;

  wire [31:0] csr_mux_out, mepc_mux_out, mcause_mux_out, mtval_mux_out,
              mepc_out, mcause_out, mtval_out, mtvec_out, mstatus_out,
              mscratch_out;

  // mepc mux
  mux2_1x32 MEPC_MUX  (.a     (csr_data_2_i),
                       .b     (nmepc_i),
                       .sel   (exc_tak_i || ret_tak_i),
                       .out   (mepc_mux_out));

  // mcause mux
  mux2_1x32 MCAUSE_MUX(.a     (csr_data_2_i),
                       .b     (next_mcause),          // Next mcause
                       .sel   (exc_tak_i || ret_tak_i),
                       .out   (mcause_mux_out));

  // mtval mux
  mux2_1x32 MTVAL_MUX (.a     (csr_data_2_i),
                       .b     (32'b0),      // trap val is always zero
                       .sel   (exc_tak_i || ret_tak_i),
                       .out   (mtval_mux_out));

  // mepc reg (Machine Excepton PC)
  reg_32 MEPC        (.clk   (clk_i),
                       .din   (mepc_mux_out),
                       .ld    (mepc_ld),
                       .dout  (mepc_out));

  // mcause reg (Machine Cause)
  reg_32 MCAUSE      (.clk   (clk_i),
                       .din   (mcause_mux_out),
                       .ld    (mcause_ld),
                       .dout  (mcause_out));

  // mtval reg (Machine Trap Value)
  reg_32 MTVAL       (.clk   (clk_i),
                       .din   (mtval_mux_out),
                       .ld    (mtval_ld),
                       .dout  (mtval_out));

  // mtvec reg (Machine Trap Vector)
  reg_32 MTVEC       (.clk   (clk_i),
                       .din   (csr_data_2_i),
                       .ld    (mtvec_ld),
                       .dout  (mtvec_out));

  // mstatus reg (Machine Status)
  mstatus_reg MSTATUS (.clk     (clk_i),
                       .din     (csr_data_2_i),
                       .priv    (priv_o),
                       .ld      (mstatus_ld),
                       .exc_tak (exc_tak_i),
                       .ret_tak (ret_tak_i),
                       .dout    (mstatus_out));

  // mscratch reg (Machine Scratch)
  reg_32 MSCRATCH     (.clk   (clk_i),
                       .din   (csr_data_2_i),
                       .ld    (mscratch_ld),
                       .dout  (mscratch_out));

  // csr mux
  mux8_1x32 CSR_MUX   (.a     (mepc_out),
                       .b     (mcause_out),
                       .c     (mtval_out),
                       .d     (mtvec_out),
                       .e     (mstatus_out),
                       .f     (mip_reg),
                       .g     (mie_reg),
                       .h     (mscratch_out),
                       .sel   (csr_addr_1_i),
                       .out   (csr_data_1_o));

  // MIE and MIP Registers
  always @(posedge clk_i) begin
  
    if (mie_ld) begin
      
      mie_reg[`MTIE] <= csr_data_2_i[`MTIE];
      mie_reg[`MEIE] <= csr_data_2_i[`MEIE];

    end

  end

  always @(*) begin

    mip_reg = 32'b0;

    mip_reg[`MTIP] = int_i[0];

    mip_reg[`MEIP] = int_i[1];

  end

  initial begin
  
    mie_reg <= 32'b0;
    mie_reg[`MTIE] <= 1'b0;
    mie_reg[`MEIE] <= 1'b0;

    priv_o <= 2'b11;

  end

  // CSR decoder
  always @(*) begin
      
      if (exc_tak_i) begin
        mepc_ld    = 1'b1;
        mcause_ld  = 1'b1;
        mtval_ld   = 1'b1;
        mtvec_ld   = 1'b0;
        mstatus_ld = 1'b0;
        mip_ld     = 1'b0;
        mie_ld     = 1'b0;
        mscratch_ld= 1'b0;
      end else if (ret_tak_i) begin
        mepc_ld    = 1'b0;
        mcause_ld  = 1'b0;
        mtval_ld   = 1'b0;
        mtvec_ld   = 1'b0;
        mstatus_ld = 1'b0;
        mip_ld     = 1'b0;
        mie_ld     = 1'b0;
        mscratch_ld= 1'b0;
      end else if (wr_en_2_i) begin
        case (csr_addr_2_i)
            `MEPC:      begin
                mepc_ld    = 1'b1;
                mcause_ld  = 1'b0;
                mtval_ld   = 1'b0;
                mtvec_ld   = 1'b0;
                mstatus_ld = 1'b0;
                mip_ld     = 1'b0;
                mie_ld     = 1'b0;
                mscratch_ld= 1'b0;
                        end
            `MCAUSE:    begin
                mepc_ld    = 1'b0;
                mcause_ld  = 1'b1;
                mtval_ld   = 1'b0;
                mtvec_ld   = 1'b0;
                mstatus_ld = 1'b0;
                mip_ld     = 1'b0;
                mie_ld     = 1'b0;
                mscratch_ld= 1'b0;
                        end
            `MTVAL:     begin
                mepc_ld    = 1'b0;
                mcause_ld  = 1'b0;
                mtval_ld   = 1'b1;
                mtvec_ld   = 1'b0;
                mstatus_ld = 1'b0;
                mip_ld     = 1'b0;
                mie_ld     = 1'b0;
                mscratch_ld= 1'b0;
                        end
            `MTVEC:     begin
                mepc_ld    = 1'b0;
                mcause_ld  = 1'b0;
                mtval_ld   = 1'b0;
                mtvec_ld   = 1'b1;
                mstatus_ld = 1'b0;
                mip_ld     = 1'b0;
                mie_ld     = 1'b0;
                mscratch_ld= 1'b0;
                        end
            `MSTATUS:   begin
                mepc_ld    = 1'b0;
                mcause_ld  = 1'b0;
                mtval_ld   = 1'b0;
                mtvec_ld   = 1'b0;
                mstatus_ld = 1'b1;
                mip_ld     = 1'b0;
                mie_ld     = 1'b0;
                mscratch_ld= 1'b0;
                        end
            `MIP:       begin
                mepc_ld    = 1'b0;
                mcause_ld  = 1'b0;
                mtval_ld   = 1'b0;
                mtvec_ld   = 1'b0;
                mstatus_ld = 1'b0;
                mip_ld     = 1'b1;
                mie_ld     = 1'b0;
                mscratch_ld= 1'b0;
                        end
            `MIE:       begin
                mepc_ld    = 1'b0;
                mcause_ld  = 1'b0;
                mtval_ld   = 1'b0;
                mtvec_ld   = 1'b0;
                mstatus_ld = 1'b0;
                mip_ld     = 1'b0;
                mie_ld     = 1'b1;
                mscratch_ld= 1'b0;
                        end
            `MSCRATCH:  begin
                mepc_ld    = 1'b0;
                mcause_ld  = 1'b0;
                mtval_ld   = 1'b0;
                mtvec_ld   = 1'b0;
                mstatus_ld = 1'b0;
                mip_ld     = 1'b0;
                mie_ld     = 1'b0;
                mscratch_ld= 1'b1;
                        end
            default:    begin
                mepc_ld    = 1'b0;
                mcause_ld  = 1'b0;
                mtval_ld   = 1'b0;
                mtvec_ld   = 1'b0;
                mstatus_ld = 1'b0;
                mip_ld     = 1'b0;
                mie_ld     = 1'b0;
                mscratch_ld= 1'b0;
                        end
        endcase
      end else begin
        mepc_ld    = 1'b0;
        mcause_ld  = 1'b0;
        mtval_ld   = 1'b0;
        mtvec_ld   = 1'b0;
        mstatus_ld = 1'b0;
        mip_ld     = 1'b0;
        mie_ld     = 1'b0;
        mscratch_ld= 1'b0;
      end

  end

  // CSR Exception control logic
  always @(*) begin
    
    case (ecause_i)
      `ECAUSE_NONE: begin
        next_mcause = 32'b0;  // X
      end
      `ECAUSE_ECALL: begin
        next_mcause = {28'b0, 1'b1, 1'b0, priv_o}; // ecall
      end
      `ECAUSE_MRET: begin
        next_mcause = 32'b0;  // X
      end
      `ECAUSE_MTIME: begin
        next_mcause = 32'b10000000000000000000000000000111;// Timer interrupt
      end
      `ECAUSE_MEXT: begin
        next_mcause = 32'b10000000000000000000000000001011;// External interrupt
      end
      `ECAUSE_INV: begin
        next_mcause = {30'b0, 2'b10}; // Illegal instruction
      end
      default: begin
        next_mcause = {30'b0, 2'b10}; // Illegal instruction
      end

    endcase

  end

  // Privilege Transition
  always @(posedge clk_i) begin
    
    if (exc_tak_i)
      priv_o <= 2'b11;
    else if (ret_tak_i)
      priv_o <= mstatus_out[12:11]; // Next privilege level

  end

  assign mtvec_o = mtvec_out;
  assign mepc_o = mepc_out;
  assign mie_o = mie_reg;
  assign mstatus_o = mstatus_out;

endmodule
