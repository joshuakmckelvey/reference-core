// A 32 bit ALU for KIRV  

`define F3_ADDI 3'b000
`define F3_SLTI 3'b010
`define F3_SLTIU 3'b011
`define F3_XORI 3'b100
`define F3_ORI  3'b110
`define F3_ANDI 3'b111

`define F3_SLLI 3'b001
`define F3_SRI  3'b101

`define F3_ADD  3'b000
`define F3_SUB  3'b000
`define F3_SLL  3'b001
`define F3_SLT  3'b010
`define F3_SLTU 3'b011
`define F3_XOR  3'b100
`define F3_SR   3'b101
`define F3_OR   3'b110
`define F3_AND  3'b111

`define SHAMT   4:0

`define F7E_STND     2'b00   // Standard ALU OP
`define F7E_SUB_A    2'b01   // Subtract or arithmetic shift
//`define `F7E_MUL_DIV  2'b10   // RV32M Instructions
`define F7E_ADD      2'b11   // Add (For Address computation)

module kirv_alu (a, b, f3, f7e, out);
  
  input [31:0] a, b;
  input [2:0] f3;
  input [1:0] f7e;

  output reg [31:0] out;

  always @(*) begin
    if (f7e != `F7E_ADD) begin
      case ( f3 )
          `F3_ADD:case ( f7e )
                      `F7E_STND: out = a + b;
                      `F7E_SUB_A: out = a - b;
                      default: out = a + b;
                  endcase
          `F3_SLL:     out = a << b[`SHAMT];
          `F3_SLT: if ( $signed(a) < $signed(b) )
                       out = 32'b1;
                   else
                       out = 32'b0;
          `F3_SLTU:if ( a < b )
                       out = 32'b1;
                   else
                       out = 32'b0;
          `F3_XOR:     out = a ^ b;
          `F3_SR: case ( f7e )
                      `F7E_STND: out = a >> b[`SHAMT];
                      `F7E_SUB_A: out = $signed(a) >>> b[`SHAMT];
                      default: out = a >> b[`SHAMT];
                  endcase
          `F3_OR:     out = a | b;
          `F3_AND:    out = a & b;
          //default: begin $display("Invalid ALU func"); $stop; end
          default: out = a + b;
      endcase
    end else begin
      
      out = a + b;

    end
  end

endmodule
