// L1 Cache Controller (L1 <-> L2)
module l1_ctrl (

  input clk_i,

  // L1 Cache/Processor input Signals
  input icache_miss_i, dcache_miss_i, dcache_dir_i, dcache_dty_i, unc_acc_i,
  input [3:0] dcache_mask_i,

  // Peripheral input valid signal
  input per_val_i,

  // L2 Line state
  input l2_hit_i,
  input l2_dty_i,

  // Processor datapath control signals
  output reg proc_stall_req_o,
  output reg proc_icache_ld,
  output reg proc_itag_val_o,
  output reg proc_dcache_acc_o,
  output reg proc_dcache_dir_o,
  output reg [3:0] proc_mask_o,
  output reg proc_dtag_val_o,
  output reg proc_unc_sel_o,

  // Peripheral control
  output reg per_en_o,          // Peripheral Enable
  output reg per_we_o,          // Peripheral Write Enable

  // L2 Cache control
  output reg l2_addr_reg_ld_o,  // Load L2 address register
  output reg l2_addr_reg_inc_o, // Increment L2 address register (by 4)
  output reg l2_we_o,           // L2 write enable
  output reg l2_val_o,          // L2 Valid output
  output reg l2_dty_o,          // L2 Dirty output

  // Processor/L1 Cache address control
  output reg proc_addr_ld_o,    // Load processor address register
  output reg proc_addr_inc_o,   // Increment processor address register (by 4)
  output reg addr_mux_sel_o,    // Selects icache/dcache address

  // L2 Controller signals
  output reg l2c_req_o,
  output reg l2_evict_o,

  output [6:0] debug_o
);

  parameter IDLE = 3'b000, DCACHE_RD_CLN = 3'b010, DCACHE_WR_CLN = 3'b011,
            DCACHE_RD_DTY = 3'b110, DCACHE_WR_DTY = 3'b111,
            ICACHE_RD = 3'b001, UNC_RD = 3'b100, UNC_WR = 3'b101;

  // Internal state registers
  reg [2:0] state;      // Current state
  reg [2:0] next_state; // Next state
  reg [4:0] stage;      // Current stage (progress) of the current state

  reg stage_res;        // Stage reset

  reg finish;           // Finish sequence

  reg per_wait;         // Halts stage until per is ready
  reg l2_wait;          // Halts stage until l2 is valid
  reg l2_wait_evict;    // Halts stage until l2 line is evicted

  assign debug_o = {state, stage};

  // Next state logic
  always @(*) begin
    
    if (state == IDLE) begin

      if (icache_miss_i) begin

        next_state = ICACHE_RD;
        stage_res = 1'b1;

      end else if (unc_acc_i) begin

        if (dcache_dir_i) begin
          next_state = UNC_WR;
          stage_res = 1'b1;
        end else begin
          next_state = UNC_RD;
          stage_res = 1'b1;
        end

      end else if (dcache_miss_i) begin

        if (dcache_dir_i) begin
          if (dcache_dty_i) begin
            next_state = DCACHE_WR_DTY;
            stage_res = 1'b1;
          end else begin
            next_state = DCACHE_WR_CLN;
            stage_res = 1'b1;
          end
        end else begin
          if (dcache_dty_i) begin
            next_state = DCACHE_RD_DTY;
            stage_res = 1'b1;
          end else begin
            next_state = DCACHE_RD_CLN;
            stage_res = 1'b1;
          end
        end

      end else begin

        next_state = IDLE;
        stage_res = 1'b1;

      end

    end else begin

      if (finish) begin
        next_state = IDLE;
        stage_res = 1'b1;
      end else begin
        next_state = state;
        stage_res = 1'b0;
      end

    end


  end

  // State transitions
  always @(posedge clk_i) begin
    
    state <= next_state;

    if (stage_res) begin

      stage <= 5'h0;

    end else if (!l2_hit_i && l2_wait) begin

      stage <= 5'h0; // Reset stage until L2 is valid

    end else if (l2_dty_i && l2_wait_evict) begin

      stage <= 5'h0; // Reset stage until L2 is valid

    end else if (!per_val_i && per_wait) begin

      stage <= stage; // Stay on current stage

    end else begin

      stage <= stage + 1;

    end

  end

  // Initializes state
  initial begin
    
    state <= IDLE;
    stage <= 5'h0;

  end

  // L2 Controller Request/Activate Line
  always @(*) begin

    if (!l2_hit_i && l2_wait) begin
      
      l2c_req_o = 1'b1;
      l2_evict_o = 1'b0;

    end else if (l2_dty_i && l2_wait_evict) begin

      l2c_req_o = 1'b1;
      l2_evict_o = 1'b1;

    end else begin

      l2c_req_o = 1'b0;
      l2_evict_o = 1'b0;

    end

  end

  // Control output logic
  always @(*) begin
    
    // Defaults
    per_wait          = 1'b0;
    l2_wait           = 1'b0;
    l2_wait_evict     = 1'b0;

    per_en_o          = 1'b0;
    proc_stall_req_o  = 1'b0;
    proc_icache_ld    = 1'b0;
    proc_itag_val_o   = 1'b0;
    proc_dcache_acc_o = 1'b0;
    proc_dcache_dir_o = 1'b0;
    proc_mask_o       = 4'b0;
    proc_dtag_val_o   = 1'b0;
    proc_unc_sel_o    = 1'b0;
    l2_addr_reg_ld_o  = 1'b0;
    l2_addr_reg_inc_o = 1'b0;
    l2_we_o           = 1'b0;
    l2_val_o          = 1'b0;
    l2_dty_o          = 1'b0;
    proc_addr_ld_o    = 1'b0;
    proc_addr_inc_o   = 1'b0;
    addr_mux_sel_o    = 1'b0;
    per_we_o          = 1'b0;
    finish            = 1'b0;

    case (state)
      IDLE: begin
      end
      ICACHE_RD: begin
        case (stage)
          5'h0: begin   // Load address
            proc_stall_req_o  = 1'b1;
            l2_addr_reg_ld_o  = 1'b1;     // Could pre-load while idle?
            proc_addr_ld_o    = 1'b1;     // Could pre-load while idle?
            addr_mux_sel_o    = 1'b0;     // icache_addr
          end
          5'h1: begin   // Wait for propagation, L2 Cache Hit Check
            proc_stall_req_o  = 1'b1;
            l2_addr_reg_inc_o = 1'b1;
            addr_mux_sel_o    = 1'b0;     // icache_addr
          end
          5'h2: begin   // Load word 0 into icache
            proc_stall_req_o  = 1'b1;
            proc_icache_ld    = 1'b1;     // icache ld
            l2_addr_reg_inc_o = 1'b1;
            proc_addr_inc_o   = 1'b1;
            l2_wait           = 1'b1;
          end
          5'h3: begin   // Load word 1 into icache
            proc_stall_req_o  = 1'b1;
            proc_icache_ld    = 1'b1;     // icache ld
            l2_addr_reg_inc_o = 1'b1;
            proc_addr_inc_o   = 1'b1;
          end
          5'h4: begin   // Load word 2 into icache
            proc_stall_req_o  = 1'b1;
            proc_icache_ld    = 1'b1;     // icache ld
            proc_addr_inc_o   = 1'b1;
          end
          5'h5: begin   // Load word 3 into icache and validate itag line
            proc_stall_req_o  = 1'b1;
            proc_icache_ld    = 1'b1;     // icache ld
            proc_itag_val_o   = 1'b1;     // Validate itag line
            finish            = 1'b1;
          end
          default: begin  // Invalid state
            proc_stall_req_o  = 1'b1;
          end
        endcase
      end
      DCACHE_WR_CLN, DCACHE_RD_CLN: begin // Can overwrite clean line
        case (stage)
          5'h0: begin   // Load address
            proc_stall_req_o  = 1'b1;
            l2_addr_reg_ld_o  = 1'b1;
            proc_addr_ld_o    = 1'b1;
            addr_mux_sel_o    = 1'b1; // dcache_addr
            proc_mask_o       = 4'b1111;  // Loads Entire Line
          end
          5'h1: begin   // Wait for propagation (Optional?) And Load next address
            proc_stall_req_o  = 1'b1;
            l2_addr_reg_inc_o = 1'b1;
            proc_mask_o       = 4'b1111;  // Loads Entire Line
          end
          5'h2: begin   // Load word 0 into dcache
            proc_stall_req_o  = 1'b1;
            proc_dcache_acc_o = 1'b1;
            proc_dcache_dir_o = 1'b1;
            l2_addr_reg_inc_o = 1'b1;
            proc_addr_inc_o   = 1'b1;
            proc_mask_o       = 4'b1111;  // Loads Entire Line
            l2_wait           = 1'b1;
          end
          5'h3: begin   // Load word 1 into dcache
            proc_stall_req_o  = 1'b1;
            proc_dcache_acc_o = 1'b1;
            proc_dcache_dir_o = 1'b1;
            l2_addr_reg_inc_o = 1'b1;
            proc_addr_inc_o   = 1'b1;
            proc_mask_o       = 4'b1111;  // Loads Entire Line
          end
          5'h4: begin   // Load word 2 into dcache
            proc_stall_req_o  = 1'b1;
            proc_dcache_acc_o = 1'b1;
            proc_dcache_dir_o = 1'b1;
            //l2_addr_reg_inc_o = 1'b1; // Not necissary?
            proc_addr_inc_o   = 1'b1;
            proc_mask_o       = 4'b1111;  // Loads Entire Line
          end
          5'h5: begin   // Load word 3 into dcache and validate dtag line
            proc_stall_req_o  = 1'b1;
            proc_dcache_acc_o = 1'b1;
            proc_dcache_dir_o = 1'b1;
            proc_dtag_val_o   = 1'b1;     // Validate dtag line
            proc_mask_o       = 4'b1111;  // Loads Entire Line
            finish            = 1'b1;
          end
          default: begin  // Invalid state
            proc_stall_req_o  = 1'b1;
          end
        endcase
      end
      DCACHE_WR_DTY, DCACHE_RD_DTY: begin // Writeback L1 (No L2 Evict yet)
        case (stage)
          5'h0: begin   // Load address
            proc_stall_req_o  = 1'b1;
            l2_addr_reg_ld_o  = 1'b1;
            proc_addr_ld_o    = 1'b1;
            addr_mux_sel_o    = 1'b1; // dcache_addr
            proc_mask_o       = 4'b1111;  // Loads Entire Line
            proc_dcache_dir_o = 1'b0;     // RD L1d
            proc_dcache_acc_o = 1'b1;
          end
          5'h1: begin   // Wait for propagation (Optional?) And Load next address
            proc_stall_req_o  = 1'b1;
            proc_addr_inc_o   = 1'b1;
            proc_mask_o       = 4'b1111;  // Loads Entire Line
            proc_dcache_dir_o = 1'b0;     // RD L1d
            proc_dcache_acc_o = 1'b1;
          end
          5'h2: begin   // Load word 0 into L2 cache
            proc_stall_req_o  = 1'b1;
            l2_addr_reg_inc_o = 1'b1;
            proc_addr_inc_o   = 1'b1;
            l2_we_o           = 1'b1;
            l2_wait_evict     = 1'b1;     // Checks L2 block for eviction
            proc_mask_o       = 4'b1111;  // Loads Entire Line
            proc_dcache_dir_o = 1'b0;     // RD L1d
            proc_dcache_acc_o = 1'b1;
          end
          5'h3: begin   // Load word 1 into L2 cache
            proc_stall_req_o  = 1'b1;
            l2_addr_reg_inc_o = 1'b1;
            proc_addr_inc_o   = 1'b1;
            l2_we_o           = 1'b1;
            proc_mask_o       = 4'b1111;  // Loads Entire Line
            proc_dcache_dir_o = 1'b0;     // RD L1d
            proc_dcache_acc_o = 1'b1;
          end
          5'h4: begin   // Load word 2 into L2 cache
            proc_stall_req_o  = 1'b1;
            l2_addr_reg_inc_o = 1'b1;
            l2_we_o           = 1'b1;
            proc_mask_o       = 4'b1111;  // Loads Entire Line
            proc_dcache_dir_o = 1'b0;     // RD L1d
            proc_dcache_acc_o = 1'b1;
          end
          5'h5: begin   // Load word 3 into L2 cache and validate L2 line
            proc_stall_req_o  = 1'b1;
            l2_we_o           = 1'b1;
            l2_val_o          = 1'b1;
            l2_dty_o          = 1'b1;     // Evicted L2 Line is dirty
            proc_dtag_val_o   = 1'b0;     // Invalidate dtag line
            proc_mask_o       = 4'b1111;  // Loads Entire Line
            proc_dcache_dir_o = 1'b1;     // Clear dty bit in L1 line
            proc_dcache_acc_o = 1'b1;
            finish            = 1'b1;
          end
          default: begin  // Invalid state
            proc_stall_req_o  = 1'b1;
          end
        endcase
      end
      UNC_RD: begin
        case (stage)
          5'h0: begin   // Load address
            per_en_o          = 1'b1;
            proc_stall_req_o  = 1'b1;
            proc_mask_o       = dcache_mask_i;
            l2_addr_reg_ld_o  = 1'b1;
            proc_addr_ld_o    = 1'b1;
            addr_mux_sel_o    = 1'b1; // dcache_addr
          end
          5'h1: begin   // Load data
            proc_stall_req_o  = 1'b1;
            proc_mask_o       = dcache_mask_i;
            per_wait          = 1'b1; // Wait for peripheral
          end
          5'h2: begin   // Store data into processor
            proc_mask_o       = dcache_mask_i;
            proc_unc_sel_o    = 1'b1;
            finish            = 1'b1;
          end
          default: begin  // Invalid state
            proc_stall_req_o  = 1'b1;
          end
        endcase
      end
      UNC_WR: begin
        case (stage)
          5'h0: begin   // Load address and data out registers
            per_en_o          = 1'b1;
            proc_stall_req_o  = 1'b1;
            proc_mask_o       = dcache_mask_i;
            l2_addr_reg_ld_o  = 1'b1;
            addr_mux_sel_o    = 1'b1; // dcache_addr
            per_we_o          = 1'b1;
          end
          5'h1: begin   // Wait for peripheral to finish
            proc_stall_req_o  = 1'b1;
            per_wait          = 1'b1; // Wait for peripheral
          end
          5'h2: begin   // Finish
            proc_unc_sel_o    = 1'b1;
            finish            = 1'b1;
          end
          default: begin  // Invalid state
            proc_stall_req_o  = 1'b1;
          end
        endcase
      end
      default: begin  // Invalid state
        proc_stall_req_o  = 1'b1;
      end
    endcase

  end


endmodule
