// Address Incrementor (by 4)
module inc4 (in, out);

  input [31:0] in;

  output wire [31:0] out;

  assign out = in + 3'b100;

endmodule
