// L2 Tag Cache (1024 lines)
module l2tag_cache (
  
  input clk_i,
  input [31:0] addr_0_i,  // Only 10 bits are used [13:4]
  input [31:0] addr_1_i,  // Only 10 bits are used [13:4]
  input [17:0] tag_0_i,   // 18 Addr bits
  input [17:0] tag_1_i,   // 18 Addr bits
  input val_0_i,          // Valid bit
  input val_1_i,          // Valid bit
  input dty_0_i,          // Dirty bit
  input dty_1_i,          // Dirty bit

  input we_0_i,
  input we_1_i,

  output [17:0] tag_o,
  output val_o,
  output dty_o

);
  
  parameter TFILE = "";   // Tag init file
  parameter VFILE = "";   // Val init file
  parameter DFILE = "";   // Dty init file

  reg [7:0] read_addr;


  mem_2rw_param     #(.FILE     (TFILE),
                        .ADDR_WIDTH (14-4),
                        .DATA_WIDTH (18))
                   TAGM(.clock    (clk_i),
                        .data_a   (tag_0_i),
                        .data_b   (tag_1_i),
                        .address_a(addr_0_i[13:4]),
                        .address_b(addr_1_i[13:4]),
                        .wren_a   (we_0_i),
                        .wren_b   (we_1_i),
                        .q_a      (tag_o),
                        .q_b      ());

  mem_2rw_param     #(.FILE     (VFILE),
                        .ADDR_WIDTH (14-4),
                        .DATA_WIDTH (1))
                   VALM(.clock    (clk_i),
                        .data_a   (val_0_i),
                        .data_b   (val_1_i),
                        .address_a(addr_0_i[13:4]),
                        .address_b(addr_1_i[13:4]),
                        .wren_a   (we_0_i),
                        .wren_b   (we_1_i),
                        .q_a      (val_o),
                        .q_b      ());

  mem_2rw_param     #(.FILE     (DFILE),
                        .ADDR_WIDTH (14-4),
                        .DATA_WIDTH (1))
                   DTYM(.clock    (clk_i),
                        .data_a   (dty_0_i),
                        .data_b   (dty_1_i),
                        .address_a(addr_0_i[13:4]),
                        .address_b(addr_1_i[13:4]),
                        .wren_a   (we_0_i),
                        .wren_b   (we_1_i),
                        .q_a      (dty_o),
                        .q_b      ());

endmodule
