// KIRV-5s processor core, L1 & L2 Cache
module kirv_core (
    
  input clk_i,
  input [1:0] int_i,

  input [31:0] mem_data_i,
  input [31:0] per_data_i,

  input mem_val_i,
  input per_val_i,

  output [31:0] mem_data_o, mem_addr_o,
  output reg mem_en_o, mem_we_o,

  output [31:0] per_data_o, per_addr_o,
  output reg per_en_o, per_we_o,
  output reg [3:0] per_be_o,

  output [31:0] pc_out_test  // For Debugging
);


  // Processor Wires/regs
  wire proc_stall_req;
  reg [31:0] proc_data_in;
  wire proc_icache_ld;
  wire proc_itag_val;
  wire proc_dcache_acc;
  wire proc_dcache_dir;
  wire [3:0] proc_mask;
  wire proc_dtag_val;
  wire unc_sel;
  wire [31:0] icache_addr;
  wire icache_miss;
  wire [31:0] proc_data_out;
  wire [31:0] dcache_addr;
  wire dcache_dir;
  wire dcache_dty;
  wire [19:0] dcache_tag;
  wire [3:0] dcache_mask;
  wire dcache_miss;
  wire [31:0] unc_data;
  wire unc_acc;

  // L1_ctrl Wires
  wire l1c_l2_addr_reg_ld;
  wire l1c_l2_addr_reg_inc;
  wire l1c_proc_addr_reg_ld;
  wire l1c_proc_addr_reg_inc;
  wire l1c_addr_mux_sel;
  wire l1c_l2_we;
  wire l1c_l2_val_in;
  wire l1c_l2_dty_in;
  wire [6:0] biu_debug;
  wire per_en;
  wire per_we;
  wire l2c_req;
  wire l2_evict;

  reg per_val;

  // L2_ctrl Wires
  wire l2c_mem_addr_reg_ld;
  wire l2c_mem_addr_reg_inc;
  wire l2c_l2_addr_reg_ld;
  wire l2c_l2_addr_reg_inc;
  wire mem_data_in_reg_ld;
  wire mem_data_out_reg_ld;
  wire l2_mux_sel;
  wire l2c_l2_we;
  wire l2c_l2_val_in;
  wire l2c_l2_dty_in;
  wire l2c_l2_tag_we;
  wire mem_en;
  wire mem_we;

  // L2 Cache Wires/Regs
  wire [17:0] l2_tag_out;
  wire l2_val_out;
  wire l2_dty_out;

  wire l1c_l2_hit;

  wire [31:0] l1c_l2_data_out;
  wire [31:0] l2c_l2_data_out;

  reg [31:0] l1c_l2_addr_reg;
  reg [31:0] l2c_l2_addr_reg;

  reg [17:0] l2_tag_mux_out;

  // Memory Bus Wires/Regs
  reg [31:0] mem_data_in_reg;
  reg [31:0] mem_data_out_reg;
  reg [31:0] mem_addr_reg;

  // Peripheral Bus Wires
  reg [31:0] per_data_in_reg;
  reg [31:0] per_data_out_reg;

  // Misc. Wires/Regs
  reg [31:0] proc_addr_reg;
  reg [31:0] l1c_proc_addr_mux_out;
  reg [31:0] l1c_l2_addr_mux_out;

  // Main datapath
  kirv_datapath DATAPATH (.clk_i            (clk_i),
                          .int_i            (int_i),
                          .ext_stall_req_i  (proc_stall_req), // Disabled
                          .ext_data_i       (proc_data_in),
                          .ext_addr_i       (proc_addr_reg),
                          .ext_icache_acc_i (proc_icache_ld),
                          .ext_itag_dir_i   (proc_itag_val),
                          .ext_dcache_acc_i (proc_dcache_acc),
                          .ext_dcache_dir_i (proc_dcache_dir),
                          .ext_mask_i       (proc_mask),
                          .ext_dcache_val_i (proc_dtag_val),
                          .ext_dcache_dty_i (1'b0),   // Always clears dty bits
                          .unc_sel_i        (unc_sel),
                          .icache_addr_o    (icache_addr),
                          .icache_miss_o    (icache_miss),
                          .dcache_data_o    (proc_data_out),
                          .dcache_addr_o    (dcache_addr),
                          .dcache_dir_o     (dcache_dir),
                          .dcache_mask_o    (dcache_mask),
                          .dcache_miss_o    (dcache_miss),
                          .dcache_dty_o     (dcache_dty),
                          .dcache_tag_o     (dcache_tag),
                          .unc_data_o       (unc_data),
                          .unc_acc_o        (unc_acc));

  // L1 Cache and Peripheral bus controller
  l1_ctrl  L1_CTRL       (.clk_i            (clk_i),
                          .icache_miss_i    (icache_miss),
                          .dcache_miss_i    (dcache_miss),
                          .dcache_dir_i     (dcache_dir),
                          .dcache_dty_i     (dcache_dty),
                          .unc_acc_i        (unc_acc),
                          .dcache_mask_i    (dcache_mask),
                          .per_val_i        (per_val),
                          .l2_hit_i         (l1c_l2_hit),
                          .l2_dty_i         (l2_dty_out),
                          .proc_stall_req_o (proc_stall_req),
                          .proc_icache_ld   (proc_icache_ld),
                          .proc_itag_val_o  (proc_itag_val),
                          .proc_dcache_acc_o(proc_dcache_acc),
                          .proc_dcache_dir_o(proc_dcache_dir),
                          .proc_mask_o      (proc_mask),
                          .proc_dtag_val_o  (proc_dtag_val),
                          .proc_unc_sel_o   (unc_sel),
                          .per_en_o         (per_en),
                          .l2_addr_reg_ld_o (l1c_l2_addr_reg_ld),
                          .l2_addr_reg_inc_o(l1c_l2_addr_reg_inc),
                          .proc_addr_ld_o   (l1c_proc_addr_reg_ld),
                          .proc_addr_inc_o  (l1c_proc_addr_reg_inc),
                          .addr_mux_sel_o   (l1c_addr_mux_sel),
                          .l2_we_o          (l1c_l2_we),
                          .l2_val_o         (l1c_l2_val_in),
                          .l2_dty_o         (l1c_l2_dty_in),
                          .debug_o          (biu_debug),  // Debug output
                          .l2c_req_o        (l2c_req),
                          .l2_evict_o       (l2_evict),
                          .per_we_o         (per_we));

  // L2 Cache and Memory bus controller
  l2_ctrl  L2_CTRL       (.clk_i            (clk_i),
                          .l1c_req_i        (l2c_req),
                          .l2_evict_i       (l2_evict || l2_dty_out), // Could be separate inputs
                          .mem_val_i        (mem_val_i),    // Unimplemented
                          .mem_en_o         (mem_en),
                          .mem_addr_reg_ld_o(l2c_mem_addr_reg_ld),
                          .mem_addr_reg_inc_o(l2c_mem_addr_reg_inc),
                          .l2_addr_ld_o     (l2c_l2_addr_reg_ld),
                          .l2_addr_inc_o    (l2c_l2_addr_reg_inc),
                          .mem_in_reg_ld_o  (mem_data_in_reg_ld),
                          .mem_out_reg_ld_o (mem_data_out_reg_ld),
                          .l2_mux_o         (l2_mux_sel),
                          .l2_we_o          (l2c_l2_we),
                          .l2_val_o         (l2c_l2_val_in),
                          .l2_dty_o         (l2c_l2_dty_in),
                          .l2_tag_we_o      (l2c_l2_tag_we),
                          .debug_o          (),           // Debug Output
                          .mem_we_o         (mem_we));

  // L2 Cache, 16KB
  l2cache      #(.FILE      ("l2_init.dat"))
        L2CACHE (.clk_i     (clk_i),
                 .addr_0_i  (l1c_l2_addr_reg),
                 .data_0_i  (proc_data_out),
                 .wr_en_0_i (l1c_l2_we),
                 .data_0_o  (l1c_l2_data_out),
                 .addr_1_i  (l2c_l2_addr_reg),
                 .data_1_i  (mem_data_in_reg),
                 .wr_en_1_i (l2c_l2_we),
                 .data_1_o  (l2c_l2_data_out));

  l2tag_cache  #(.VFILE     ("l2_init_val.dat"),
                 .TFILE     ("l2_init_tag.dat"),
                 .DFILE     ("l2_init_dty.dat"))
         L2TAG  (.clk_i     (clk_i),            // TODO: Implement l1c/l2c mux
                 .addr_0_i  (l1c_l2_addr_reg),  // Currently, only l1c uses bits
                 .addr_1_i  (l2c_l2_addr_reg),  // Currently, only l2c writes
                 .tag_0_i   (l1c_l2_addr_reg[31:14]), // Uses 18 tag bits
                 .val_0_i   (l1c_l2_val_in),
                 .tag_1_i   (l2c_l2_addr_reg[31:14]), // Uses 18 tag bits
                 .val_1_i   (l2c_l2_val_in),
                 .dty_0_i   (l1c_l2_dty_in),
                 .dty_1_i   (l2c_l2_dty_in),
                 .we_0_i    (l1c_l2_we),
                 .we_1_i    (l2c_l2_tag_we),//l1c_l2_tag_we),
                 .tag_o     (l2_tag_out),
                 .val_o     (l2_val_out),
                 .dty_o     (l2_dty_out));

  assign l1c_l2_hit = l2_val_out && (l1c_l2_addr_reg[31:14] == l2_tag_out);

  // Processor Address register
  always @(posedge clk_i) begin
    
    if (l1c_proc_addr_reg_ld) begin
      
      if (!unc_acc)
        proc_addr_reg <= {l1c_proc_addr_mux_out[31:4], 4'b0}; // line aligned access
      else
        proc_addr_reg <= l1c_proc_addr_mux_out;

    end else if (l1c_proc_addr_reg_inc) begin

      proc_addr_reg <= proc_addr_reg + 4;

    end

  end

  // L1 Controller L2 Address register
  always @(posedge clk_i) begin
    
    if (l1c_l2_addr_reg_ld) begin
      
      if (unc_acc)
        l1c_l2_addr_reg <= l1c_l2_addr_mux_out;
      else
        l1c_l2_addr_reg <= {l1c_l2_addr_mux_out[31:4], 4'b0}; // line aligned access

    end else if (l1c_l2_addr_reg_inc) begin

      l1c_l2_addr_reg <= l1c_l2_addr_reg + 4;

    end

  end

  // L2 Controller L2 Address register
  always @(posedge clk_i) begin
    
    if (l2c_l2_addr_reg_ld) begin
      
      l2c_l2_addr_reg <= {l1c_l2_addr_reg[31:4], 4'b0}; // line aligned access

    end else if (l2c_l2_addr_reg_inc) begin

      l2c_l2_addr_reg <= l2c_l2_addr_reg + 4;

    end

  end

  // Memory Address register
  always @(posedge clk_i) begin
    
    if (l2c_mem_addr_reg_ld) begin
      
      // Writeback from L2->Mem uses addr of block being evicted
      if (mem_we)
        mem_addr_reg <= {l2_tag_out, l1c_l2_addr_reg[13:4], 4'b0}; // line aligned access
      else
        mem_addr_reg <= {l1c_l2_addr_reg[31:4], 4'b0}; // line aligned access

    end else if (l2c_mem_addr_reg_inc) begin

      mem_addr_reg <= mem_addr_reg + 4;

    end

  end

  // L1 Controller Processor Address mux
  always @(*) begin

    if (l1c_addr_mux_sel == 1'b1)
      if (unc_sel)  // Or rename to: proc_data_in_sel
        l1c_proc_addr_mux_out = dcache_addr;
      else
        l1c_proc_addr_mux_out = {dcache_addr[31:4], 4'b0};
    else
      l1c_proc_addr_mux_out = {icache_addr[31:4], 4'b0};

  end

  // L1 Controller L2 Address mux
  always @(*) begin

    if (l1c_addr_mux_sel == 1'b1)
      if (dcache_dty)
        l1c_l2_addr_mux_out = {dcache_tag, dcache_addr[11:4], 4'b0};
      else
        l1c_l2_addr_mux_out = {dcache_addr[31:4], 4'b0};
    else
      l1c_l2_addr_mux_out = {icache_addr[31:4], 4'b0};

  end

  // L2 Cache Mux (Selects between L1 controller and L2 controller)
  // Currently Unused
  always @(*) begin

    if (l2_mux_sel)
      l2_tag_mux_out = l2c_l2_addr_reg[31:14];
    else
      l2_tag_mux_out = l1c_l2_addr_reg[31:14];

  end

  assign mem_addr_o = mem_addr_reg;
  assign per_addr_o = dcache_addr;

  // Processor Data in mux
  always @(*) begin
    
    if (unc_sel)  // Or rename to: proc_data_in_sel
      proc_data_in = per_data_in_reg;
    else
      proc_data_in = l1c_l2_data_out;

  end

  // Memory Data in register
  always @(posedge clk_i) begin
    
    if (mem_data_in_reg_ld) begin
      
      //if (mem_en)
        mem_data_in_reg <= mem_data_i;

    end

  end

  // Memory Data out register
  always @(posedge clk_i) begin
    
    if (mem_data_out_reg_ld) begin
      
      mem_data_out_reg <= 32'b0;      // Not implemented yet

    end

  end

  // Peripheral Data in register
  always @(posedge clk_i) begin
    
    //if (per_data_in_reg_ld) begin
      
      //if (per_en)
        per_data_in_reg <= per_data_i;

    //end

  end

  // Peripheral Data out register
  always @(posedge clk_i) begin
    
    per_data_out_reg <= unc_data;


  end

  assign mem_data_o = l2c_l2_data_out;//mem_data_out_reg;
  assign per_data_o = per_data_out_reg;

  // Registered Inputs
  always @(posedge clk_i) begin
    
    per_val <= per_val_i;
      
  end

  // Registered Outputs
  always @(posedge clk_i) begin
    
    mem_en_o <= mem_en;
    per_en_o <= per_en;

    mem_we_o <= mem_we;
    per_we_o <= per_we;
    per_be_o <= proc_mask;

  end

  // Initial registered output values
  initial begin
    
    mem_en_o <= 1'b0;
    per_en_o <= 1'b0;

    mem_we_o <= 1'b0;
    per_we_o <= 1'b0;
    per_be_o <= 4'b0;

  end

  //assign pc_out_test = {icache_miss, dcache_miss, icache_addr[29:0]};
  assign pc_out_test = {icache_miss, dcache_miss, 3'b0, biu_debug, icache_addr[19:0]};

endmodule
